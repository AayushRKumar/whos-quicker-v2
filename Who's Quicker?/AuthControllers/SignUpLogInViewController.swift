//
//  SignUpLogInViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/9/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import AuthenticationServices
import CryptoKit
class SignUpLogInViewController: UIViewController, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding  {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
   
    
     var alertController = UIAlertController()
    @IBOutlet weak var buttonTwoWeak: UIButton!
    @IBOutlet weak var buttonOneWeak: UIButton!
    @IBOutlet weak var loginToLabel: UILabel!
  
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
      
        buttonOneWeak.layer.cornerRadius = 20
        buttonOneWeak.clipsToBounds = true
        buttonTwoWeak.layer.cornerRadius = 20
        buttonTwoWeak.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(segueView), name: Notification.Name(rawValue: "segueToMain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkAccountInDatabase), name: Notification.Name(rawValue: "checkAccountInDatabse"), object: nil)
        self.buttonOneWeak.isHidden = true
        self.buttonTwoWeak.isHidden = true
       
        self.loginToLabel.isHidden = true
        titleLabel.text = "Loading"
      checkLoggedIn()
       
        // Do any additional setup after loading the view.
    }
    func checkLoggedIn() {
        if Auth.auth().currentUser != nil {
            
          //  checkDoublePlayer()
            //affected by "users"
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
            
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                if snapshot.exists() {
                   
                    let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
                    refToCheck.observeSingleEvent(of: .value)  { (snapshot) in
                        guard let check = snapshot.childSnapshot(forPath:"Suspended").value as? Bool else { return }
                        guard let msg = snapshot.childSnapshot(forPath:"SuspendedMsg").value as? String else { return }
                        if !check {
                            
                            self.buttonOneWeak.isHidden = false
                            self.buttonTwoWeak.isHidden = false
                            self.loginToLabel.isHidden = false
                            self.segueView()
                        } else {
                            self.buttonOneWeak.isEnabled = false
                            let alert = UIAlertController(title: "Your Account has been Suspended", message: msg , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                            self.present(alert, animated: true)
                        }
                    }
                    //continue
                    self.titleLabel.text = "Who's Quicker?"
                    self.buttonOneWeak.isEnabled = true
                    
                    self.buttonTwoWeak.isEnabled = true
                } else {
                    self.titleLabel.text = "Who's Quicker?"
                    self.buttonOneWeak.isHidden = false
                   // self.buttonTwoWeak.isHidden = false
                   
                    self.loginToLabel.isHidden = false
                }
            }
          // User is signed in.
          // ...
        } else {
            print("a")
          // No user is signed in.
          // ...
            self.titleLabel.text = "Who's Quicker?"
            self.buttonOneWeak.isHidden = false
           // self.buttonTwoWeak.isHidden = false
           
            self.loginToLabel.isHidden = false
            
        }
    }
    func checkDoublePlayer()  {
        if Auth.auth().currentUser == nil { return }
        let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "UsersINGORE")
        refToCheck.observeSingleEvent(of: .value) { snapshot in
            if snapshot.hasChild("\((Auth.auth().currentUser!.uid))") {
                
            let refToCheck2 = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
                refToCheck2.observeSingleEvent(of: .value) { snapshot2 in
                    for item in snapshot2.children {
                       
                        guard let user = item as? DataSnapshot else { break }
                       
                        guard let dict = user.value as? Dictionary<String, Any> else {  break }
                        guard let index = dict.index(forKey: "Status") else {  break }
                        
                        let status = dict[index].value as! String
                         
                        if (status == "1") {
                            let ac = UIAlertController(title: "You Are Already Signed in on Another Device!", message: nil, preferredStyle: .alert)
                            let refresh = UIAlertAction(title: "Refresh", style: .default) { [self] _ in
                                self.checkLoggedIn()
                                
                            }
                            ac.addAction(refresh)
                            self.present(ac, animated: true)
                            
                        } else {
                           
                        }
                      
                       
                    }
                    
                }
             
            }
        }
    }

    @IBAction func signUp(_ sender: Any) {
        let alert = UIAlertController(title: "Continue With...", message: "\n \n \n \n \n \n \n ", preferredStyle: .alert)
       
        
        let button = GIDSignInButton(frame: CGRect(origin: CGPoint(x: 10, y: 50), size: CGSize(width: 250, height: 30)))
        button.addTarget(SignUpLogInViewController(), action: #selector(googleSign), for: .touchUpInside)
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        alert.view.addSubview(button)
        let appleButton = ASAuthorizationAppleIDButton(frame: CGRect(origin: CGPoint(x: 10, y: 150), size: CGSize(width: 250, height: 48)))
        appleButton.frame=CGRect(origin: CGPoint(x: 10, y: 100), size: CGSize(width: 250, height: 48))
        appleButton.addTarget(SignUpLogInViewController(), action: #selector(appleSignIn), for: .touchUpInside)
        alert.view.addSubview(appleButton)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController = alert
        present(alert, animated: true)

    }
    @objc func googleSign() {
        print("done")
        alertController.dismiss(animated: true)
        //segue
        //maybe for user data just have a database or firestore full of UUID (mine) wins wins wins wins etc
    }
    @IBAction func logIn(_ sender: Any) {
        let alert = UIAlertController(title: "Log in With...", message: "\n \n \n \n", preferredStyle: .alert)
       
        
        let button = GIDSignInButton(frame: CGRect(origin: CGPoint(x: 10, y: 50), size: CGSize(width: 250, height: 20)))
        button.addTarget(SignUpLogInViewController(), action: #selector(googleSign), for: .touchUpInside)
        alert.view.addSubview(button)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController = alert
        present(alert, animated: true)
    }
     @objc public func segueView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")

                 secondVC.modalPresentationStyle = .fullScreen
         secondVC.modalTransitionStyle = .crossDissolve
     
      present(secondVC, animated: true, completion: nil)
    }
    
    @objc func checkAccountInDatabase() {
        
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let secondVC = storyboard.instantiateViewController(identifier: "makeUsernameViewController")

                         secondVC.modalPresentationStyle = .fullScreen
                 secondVC.modalTransitionStyle = .crossDissolve
                         //fade to black on next view
                 self.present(secondVC, animated: true, completion: nil)
            
        
    }
    
    
    //new apple stuff
    
    @objc func appleSignIn() {
        alertController.dismiss(animated: true)
        startSignInWithAppleFlow()
    }
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    fileprivate var currentNonce: String?
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
      let nonce = randomNonceString()
      currentNonce = nonce
      let appleIDProvider = ASAuthorizationAppleIDProvider()
      let request = appleIDProvider.createRequest()
   //   request.requestedScopes = [.fullName, .email]
      request.nonce = sha256(nonce)

      let authorizationController = ASAuthorizationController(authorizationRequests: [request])
      authorizationController.delegate = self
      authorizationController.presentationContextProvider = self
      authorizationController.performRequests()
    }

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
          guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
          }
          guard let appleIDToken = appleIDCredential.identityToken else {
            print("Unable to fetch identity token")
            return
          }
          guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
            return
          }
          // Initialize a Firebase credential.
          let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                    idToken: idTokenString,
                                                    rawNonce: nonce)
          // Sign in with Firebase.
          Auth.auth().signIn(with: credential) { (authResult, error) in
            if (error != nil) {
              // Error. If error.code == .MissingOrInvalidNonce, make sure
              // you're sending the SHA256-hashed nonce as a hex string with
              // your request to Apple.
                print(error!.localizedDescription)
              return
            }
           // let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users")
            //affected by "users"
//            ref.observeSingleEvent(of: .value)  { (snapshot) in
//                if snapshot.hasChild("\(String(describing: Auth.auth().currentUser!.uid))")
//                {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "segueToMain"), object: nil)
//                } else {
//
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "checkAccountInDatabse"), object: nil)
//                }
//            }
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(String(describing: Auth.auth().currentUser!.uid))")
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                
                           if snapshot.hasChildren()
                           {
                               NotificationCenter.default.post(name: Notification.Name(rawValue: "segueToMain"), object: nil)
                           } else {
           
                               NotificationCenter.default.post(name: Notification.Name(rawValue: "checkAccountInDatabse"), object: nil)
                           }
                       }
            
            // User is signed in to Firebase with Apple.
            // ...
          }
        }
      }

      func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
      }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
