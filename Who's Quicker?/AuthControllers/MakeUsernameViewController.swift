//
//  MakeUsernameViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/15/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase

class MakeUsernameViewController: UIViewController {
 var username = ""
    var passed = false
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var warningLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        warningLabel.text = ""
        let bb = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(done))
        textField.addNumericAccessory(addPlusMinus: false, bb: bb)
        textField.becomeFirstResponder()
        textField.autocorrectionType = .no
        // Do any additional setup after loading the view.
    }
    @IBAction func textFieldDidChange(_ sender: Any) {
      
    }
    
    @IBAction func editingChanged(_ sender: Any) {
        if textField.text?.count ?? 1 < 3 || textField.text?.count ?? 1 > 12 {
            warningLabel.text = "Usernames must be 3 - 12 characters long"
            passed = false
            return
        }
        
       
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-")
        if textField.text!.rangeOfCharacter(from: characterset.inverted) != nil {
            warningLabel.text = "Usernames cannot contain special characters other than  -  and  _  "
            passed = false
            return
        }
        if textField.text!.aa_profanityFiltered().contains("*") {
            warningLabel.text = "Usernames cannot contain Profanity"
            passed = false
            return
        }
        username = textField.text!
        warningLabel.text = ""
        passed = true
    }
    @objc func done() {
        if passed {
//            let reftoCheck = Database.database(url: "https://who-s-quicker-usernames.firebaseio.com/").reference(withPath: "usernames")
//            reftoCheck.observeSingleEvent(of: .value)  { (snapshot) in
//                let dict = snapshot.value as! Dictionary<String, Any>
//
//                for key in dict.keys {
//                    if self.username == key {
//                        self.warningLabel.text = "This Username is already Taken"
//                        return
//                    }
//                }
            
            let reftoCheck = Database.database(url: "https://who-s-quicker-usernames.firebaseio.com/").reference(withPath: "usernames/\(username.lowercased())")
            
                        reftoCheck.observeSingleEvent(of: .value)  { (snapshot) in
                            
                            if snapshot.exists() {
                                //there is a username
                                self.warningLabel.text = "This Username is already Taken"
                                    return
                                
                            } else {
                                print("good username")
                                // no other username of the same name

                            }
                          //affected by "users"
                          let ac = UIAlertController(title: "Confirmation", message: "Are you sure you want your username to be \(self.username)?", preferredStyle: .alert)
                            ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: .none))
                           
                            let yes = UIAlertAction(title: "Yes", style: .default) {  [self] _ in
                            let reftoAdd = Database.database(url: "https://who-s-quicker-usernames.firebaseio.com/").reference(withPath: "usernames/\(self.username.lowercased())")
                reftoAdd.setValue(0)
        
                                let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
                                
               
                            ref.setValue(["Username": "\(self.username)", "Math Wins": 0, "CW Wins": 0, "CM Wins": 0, "TB Wins": 0, "UUID": "\(NSUUID().uuidString)", "Friends" : [], "Suspended": false, "SuspendedMsg" : "", "Status" : "1"])
                uuid = self.username
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")

                         secondVC.modalPresentationStyle = .fullScreen
                 secondVC.modalTransitionStyle = .crossDissolve
             
                self.present(secondVC, animated: true, completion: nil)
                            
                            }
                            
                            ac.addAction(yes)
                            self.present(ac, animated: true, completion: nil)
            }
            
            
       
        }
        
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
