//
//  CMViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/30/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
class CMViewController: UIViewController {
   
    var selectedButton_row = ""
    var selectedButton_color = UIColor()
    var selectedButton = UIButton()
    var HMM = 0
    var selectedAlready = false
    var timer = Timer()
    var connectivityTimer = Timer()
    let colorArray = [UIColor(red: 1.00, green: 0.45, blue: 0.43, alpha: 1), UIColor(red: 0.46, green: 1.00, blue: 0.62, alpha: 1),
                      UIColor(red: 0.37, green: 0.96, blue: 1.00, alpha: 1), UIColor(red: 0.95, green: 0.78, blue: 0.98, alpha: 1), UIColor(red: 1.00, green: 0.98, blue: 0.50, alpha: 1)
    ]
    var countCM = 0
    var roundsA = [[Int]]()
    var roundsB = [[Int]]()
    var roundCounter = 0
    var numberOfRounds = 0
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
        timeLabel.adjustsFontSizeToFitWidth = true
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData")
        
    
    

        
        
        ref.observeSingleEvent(of: .value) {  snapshot in
          //  ref.observe(.value) { [self] snapshot in
            
        let roundRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
            let dict = snapshot.value as! Dictionary<String, Any>
                
            self.numberOfRounds = dict.keys.count / 2
            
            let tRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData")
           tRef.observeSingleEvent(of: .value) { snapshot2 in
               let value = snapshot2.value as! Dictionary<String, [Int]>
               
            
            for index in 1...self.numberOfRounds {
                let currentRoundA = value["\(index)a"] ?? [0, 1, 2, 3, 4]
                self.roundsA.append(currentRoundA)
                
                let currentRoundB = value["\(index)b"] ?? [0, 1, 2, 3, 4]
                self.roundsB.append(currentRoundB)
                
            }
//           var counter = false
//            var gen = SeededRandomNumberGenerator(seed: matchPath.count)
//                for b in a.values {
//
//
//
//                    if !counter {
//
//                        self.roundsA.append(b)
//                        counter = true
//                    } else if counter {
//
//                        self.roundsB.append(b)
//                        counter = false
//                    }
//
//                }
//
//
//            self.roundsA = self.roundsA.sorted(by: { $0[0] > $1[0] })
//            self.roundsB = self.roundsB.sorted(by: { $0[0] > $1[0] })
//
////            for (index, var round) in self.roundsA.enumerated() {
////                round.sort()
////                self.roundsA.insert(round, at: index)
////            }
////            for (index, var round) in self.roundsB.enumerated() {
////                round.sort()
////                self.roundsB.insert(round, at: index)
////            }
//            print(self.roundsA)
//            print(self.roundsB)
//            self.roundsA.shuffle(using: &gen)
//            self.roundsB.shuffle(using: &gen)
//            print(self.roundsA)
//            print(self.roundsB)
            
            
            let alert = UIAlertController(title: "Game Data (For Testing Purposes)", message: "\(self.roundsA) \n \(self.roundsB)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
//            let alert = UIAlertController(title: "info", message: "\(self.roundsA) | \(self.roundsB)", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "yes", style: .cancel, handler: nil))
//
//           
//           
//            self.present(alert, animated: true)
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateTimer), userInfo: nil, repeats: true)
            self.setButtonColors()
            
            //start game
            
           }
           
//
//            for i in 1...self.numberOfRounds {
//
//
//
//                let countingRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData/\(i)a")
//                countingRef.observeSingleEvent(of: .value) { snapshot1 in
//                    self.roundsA.append(snapshot1.value as! [Int])
//
//                }
//                let countingRef2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData/\(i)b")
//                countingRef2.observeSingleEvent(of: .value) { snapshot1 in
//                    self.roundsB.append(snapshot1.value as! [Int])
//                }
//
//            }
           
            self.countCM = 0
            playerTime = 0.0
            //Setting Up
            
           
            
            let connref = Database.database().reference(withPath: "connectWaitTime")
            var waitTime = 5
            connref.observeSingleEvent(of: .value) { [self] snapshot in
                //connref.observe(.value) { [self] snapshot in
                waitTime = snapshot.value as! Int
            }
            self.connectivityTimer = Timer.scheduledTimer(timeInterval: TimeInterval(waitTime), target: self, selector: #selector(self.updateConnectivity), userInfo: nil, repeats: true)
            self.roundCounter = 1
            self.roundLabel.text = "Round \(self.roundCounter) of \(self.numberOfRounds)"
            
            
           
        }
    }
    @objc func updateConnectivity() {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) connected")
        ref.setValue(true)
        
    }
    @objc func UpdateTimer() {
        
        playerTime += 0.1
        timeLabel.text = "\(String(format: "%.1f", playerTime)) seconds"
        
    }
    @IBOutlet weak var button1a: UIButton!
    
    @IBOutlet weak var button2a: UIButton!
    
    @IBOutlet weak var button3a: UIButton!
    
    @IBOutlet weak var button4a: UIButton!
    
    @IBOutlet weak var button5a: UIButton!
    
    @IBOutlet weak var button1b: UIButton!
    
    @IBOutlet weak var button2b: UIButton!
    
    @IBOutlet weak var button3b: UIButton!
    
    @IBOutlet weak var button4b: UIButton!
    
    @IBOutlet weak var button5b: UIButton!
    
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    func setUpView() {
        let screenHeight = UIScreen.main.bounds.height
        let screenWidth = UIScreen.main.bounds.width
        let buttonArrayA = [button1a, button2a, button3a, button4a, button5a]
        let buttonArrayB = [button1b, button2b, button3b, button4b, button5b]
        
        let spacerSize = screenHeight / 11
        var spacerArray = [Int]()
        var currMarker = 0
        for numb in 0...11 {
            currMarker += Int(spacerSize)
            spacerArray.append(currMarker)
        }
        
       
        let widthSize = (screenWidth - (screenWidth/20) - (screenWidth/4)) / 3
        
        var index = 0
        for button in buttonArrayA {
            
            
            
            button?.isHidden = false
            button?.frame = CGRect(x: 20, y: CGFloat(spacerArray[index]), width: widthSize, height: spacerSize)
            
            index += 2
            
            clearButtonBorder(button!)
            
        }
        index = 0
        for button in buttonArrayB {
//            button?.backgroundColor = colorArray[colorsB[index / 2]]
            button?.isHidden = false
            button?.frame = CGRect(x: screenWidth - 20 - widthSize, y: CGFloat(spacerArray[index]), width: widthSize, height: spacerSize)
           
            index += 2
            
           clearButtonBorder(button!)
        }
        
        roundLabel.center.x = screenWidth / 2
        timeLabel.center.x = screenWidth / 2
        
        roundLabel.center.y = (button1a.center.y + button2a.center.y) / 2
        timeLabel.center.y = (button2a.center.y + button3a.center.y) / 2
       
        
    }
    
    func setButtonColors() {
        
        let aSideColors = roundsA[countCM]
        let bSideColors = roundsB[countCM]
        
        let buttonArrayA = [button1a, button2a, button3a, button4a, button5a]
        let buttonArrayB = [button1b, button2b, button3b, button4b, button5b]
        
        for (index, button) in buttonArrayA.enumerated() {
            button?.backgroundColor = colorArray[aSideColors[index]]
        }
        for (index, button) in buttonArrayB.enumerated() {
            button?.backgroundColor = colorArray[bSideColors[index]]
        }
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        setUpView()
    }
    
    func clearButtonBorder(_ button:UIButton) {
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
    }
//if they press the button that was alreasy pressed it gets desecleted
    
    @IBAction func colorButtonPressed(_ sender: UIButton) {
        
        
        if !selectedAlready {
            selectedAlready = true
            sender.layer.borderWidth = 10
          //  sender.layer.borderColor = UIColor(red: 0.22, green: 0.27, blue: 0.90, alpha: 1).cgColor
            selectedButton_color = sender.backgroundColor!
            selectedButton = sender
        } else if selectedAlready {
            selectedAlready = false
            if sender == selectedButton {
              clearButtonBorder(sender)
            } else {
               
                sender.layer.borderWidth = 10
             //   sender.layer.borderColor = UIColor(red: 0.22, green: 0.27, blue: 0.90, alpha: 1).cgColor
                if selectedButton_color == sender.backgroundColor {
                    clearButtonBorder(sender)
                    clearButtonBorder(selectedButton)
                    sender.isHidden = true
                    selectedButton.isHidden = true
                    
                    HMM += 1
                    
                    if HMM == 5 {
                        countCM += 1
                        if numberOfRounds == countCM {
                          
                            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
                            ref.child("\(uuid) time").setValue(playerTime)
                            timer.invalidate()
                            connectivityTimer.invalidate()
                             var timeExists = Bool()
                           
                            
                            ref.observeSingleEvent(of: .value)  { (snapshot) in
                                // ref.observe(.value)  { (snapshot) in
                                
                                for child in snapshot.children {
                                    
                                    let str = String(describing: child)
                                    if str.contains("\(opponentUUID) time") {
                                        timeExists = true
                                        
                                        break;
                                    } else {
                                        
                                        timeExists = false
                                    }
                                    
                                }
                                self.readTime(timeExists: timeExists)
                            }
                            
                        } else {
                         setUpView()
                            HMM = 0
                            roundCounter += 1
                            self.roundLabel.text = "Round \(roundCounter) of \(self.numberOfRounds)"
                        setButtonColors()
                            
                        }
                    }
                } else {
                    clearButtonBorder(sender)
                    clearButtonBorder(selectedButton)
                    
                }
                
            }
            
            
            
        }
    }
    func readTime(timeExists: Bool) {
        print("readtimeexists \(timeExists)")
        if !timeExists {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "WaitingController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        }
    }
 
}
