//
//  CWViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/4/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
class CWViewController: UIViewController {
   
    @IBOutlet weak var wordCounterLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var wordLabel: UILabel!
    
    @IBOutlet weak var answerTextField: UITextField!
    
    var listOfWords = [String]()
    var countCW = 0
    var timer = Timer()
  
    var connectivityTimer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        answerTextField.becomeFirstResponder()
        answerTextField.autocorrectionType = .no
        //We need a seed or something
        view.backgroundColor = chosenGameColor
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData")
        
        if UIDevice.current.model.hasPrefix("iPad") {
            self.view.frame.origin.y -= 20
           
        }
    

        
        
        ref.observeSingleEvent(of: .value) { [self] snapshot in
          //  ref.observe(.value) { [self] snapshot in
            let a = snapshot.value as! Dictionary<String, String>
            let array = a.keys
            
            
            for item in array {
                self.listOfWords.append(item)
                
            }
            self.countCW = 0
            playerTime = 0.0
            //Setting Up
            self.wordLabel.text = "\(self.listOfWords[self.countCW])"
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateTimer), userInfo: nil, repeats: true)
            
            let connref = Database.database().reference(withPath: "connectWaitTime")
            var waitTime = 5
            connref.observeSingleEvent(of: .value) { [self] snapshot in
                //connref.observe(.value) { [self] snapshot in
                waitTime = snapshot.value as! Int
            }
            self.connectivityTimer = Timer.scheduledTimer(timeInterval: TimeInterval(waitTime), target: self, selector: #selector(self.updateConnectivity), userInfo: nil, repeats: true)
            
            self.wordCounterLabel.text = "Word 1 of \(self.listOfWords.count)"
        }
        let bb = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(enterAnswer))
        answerTextField.addNumericAccessory(addPlusMinus: false, bb: bb)
//
//               let toolBar = UIToolbar(frame: CGRect(x: 0.0,
//                   y: 0.0,.action
        
//             width: UIScreen.main.bounds.size.width,
//               height: 44.0))//1
//               let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
//        flexible.tintColor = chosenGameColor
//        let barButton = UIBarButtonItem(title: "Enter", style: .plain, target: target, action: #selector(done))//3
//
//
//        let barButton2 = UIBarButtonItem(title: "Negative (-)", style: .plain, target: target, action: #selector(done))//3
//
//
//       // barButton3.isEnabled = false
//
//
//               toolBar.setItems([barButton2, flexible, barButton], animated: false)//4
//
//        AnswerTextField.inputAccessoryView = toolBar
        // Do any additional setup after loading the view.
        
    }
    @objc func UpdateTimer() {
        
        playerTime += 0.1
        
        timeLabel.text = "\(String(format: "%.1f", playerTime)) seconds"
        
    }
    @objc func updateConnectivity() {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) connected")
        ref.setValue(true)
        
       // let ref2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
        
        
    }
    @objc func enterAnswer() {
        let premAnswer = (answerTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
        if premAnswer == "" { return }
        
        let answer = premAnswer!
        var correctAnswerGiven = false
        answerTextField.text = ""
        if answer == listOfWords[countCW] {
            //right
            correctAnswerGiven = true
            countCW += 1
        } else {
            //wrong
            correctAnswerGiven = false
        }
        if correctAnswerGiven {
            wordCounterLabel.text = "Correct!"
            wordCounterLabel.textColor = UIColor(red: 120/256, green: 255/256, blue: 115/256, alpha: 1)
        } else {
            wordCounterLabel.text = "Incorrect!"
            wordCounterLabel.textColor = UIColor(red: 255/256, green: 115/256, blue: 120/256, alpha: 1)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.wordCounterLabel.text = "Word \(self.countCW + 1) of \(self.listOfWords.count)"
            self.wordCounterLabel.textColor = UIColor.black
        }
        if countCW == listOfWords.count {
            
            //end game
            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
            ref.child("\(uuid) time").setValue(playerTime)
            timer.invalidate()
            connectivityTimer.invalidate()
             var timeExists = Bool()
          
            
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                // ref.observe(.value)  { (snapshot) in
                
                for child in snapshot.children {
                    
                    let str = String(describing: child)
                    if str.contains("\(opponentUUID) time") {
                        timeExists = true
                       
                        break;
                    } else {
                        
                        timeExists = false
                    }
                    
                }
                self.readTime(timeExists: timeExists)
            }
        } else {
            wordLabel.text = listOfWords[countCW]
        }
    }
    func readTime(timeExists: Bool) {
        print("readtimeexists \(timeExists)")
        if !timeExists {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "WaitingController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
