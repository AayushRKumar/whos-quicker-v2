//
//  MathViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/16/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
var matchPath = ""
class MathViewController: UIViewController {
 let QuestionsArray = [String]()
    var CountMath = 0
    var listOfQuestions = [String]()
    var timer = Timer()
   var musicTimer = Timer()
    var connectivityTimer = Timer()
   var musicPlayer = AVAudioPlayer()
    @IBOutlet weak var AnswerTextField: UITextField!
    @IBOutlet var QuestionLabel: UILabel!
    @IBOutlet var TimeLabel: UILabel!
   
    @IBOutlet var QuestionCounterLabel: UILabel!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        connectivityTimer.invalidate()
        timer.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnswerTextField.becomeFirstResponder()
        
        //We need a seed or something
      
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData")
        
       
    

        
        
        ref.observeSingleEvent(of: .value) { [self] snapshot in
          //  ref.observe(.value) { [self] snapshot in
            let a = snapshot.value as! Dictionary<String, Any>
            let array = a.keys
            
            
            for item in array {
                self.listOfQuestions.append(item)
                
            }
            self.CountMath = 0
            playerTime = 0.0
            //Setting Up
            self.QuestionLabel.text = "\(self.listOfQuestions[self.CountMath]) ="
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateTimer), userInfo: nil, repeats: true)
            
            let connref = Database.database().reference(withPath: "connectWaitTime")
            var waitTime = 5
            connref.observeSingleEvent(of: .value) { [self] snapshot in
                //connref.observe(.value) { [self] snapshot in
                waitTime = snapshot.value as! Int
            }
            self.connectivityTimer = Timer.scheduledTimer(timeInterval: TimeInterval(waitTime), target: self, selector: #selector(self.updateConnectivity), userInfo: nil, repeats: true)
            
            self.QuestionCounterLabel.text = "Question 1 of \(self.listOfQuestions.count)"
        }
        let bb = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(enterAnswer))
        let item = AnswerTextField.inputAssistantItem
            item.leadingBarButtonGroups = []
            item.trailingBarButtonGroups = []
     //maybe try to put a textfield where clear is if its ipad
        AnswerTextField.addNumericAccessory(addPlusMinus: true, bb: bb)
        AnswerTextField.inputView = NumericKeyboard(target: AnswerTextField)
        if UIDevice.current.model.hasPrefix("iPad") {
            print("datdat")
          //  AnswerTextField.inputView = NumericKeyboard(target: enterAnswer())
           
         //   AnswerTextField.center.y = AnswerTextField.center.y - (UIScreen.main.bounds.height/10) - 20
        
        } else {
           
        }
        
        
//
//               let toolBar = UIToolbar(frame: CGRect(x: 0.0,
//                   y: 0.0,
//             width: UIScreen.main.bounds.size.width,
//               height: 44.0))//1
//               let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
//        flexible.tintColor = chosenGameColor
//        let barButton = UIBarButtonItem(title: "Enter", style: .plain, target: target, action: #selector(done))//3
//
//
//        let barButton2 = UIBarButtonItem(title: "Negative (-)", style: .plain, target: target, action: #selector(done))//3
//
//
//       // barButton3.isEnabled = false
//
//
//               toolBar.setItems([barButton2, flexible, barButton], animated: false)//4
//
//        AnswerTextField.inputAccessoryView = toolBar
        // Do any additional setup after loading the view.
        
        let FileLocation = Bundle.main.path(forResource: "music", ofType: "wav")
        do {
           
            
            
            musicPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: FileLocation!) as URL)
           
            
        } catch  { }
           
        
        
        musicTimer = Timer.scheduledTimer(timeInterval: 3.4, target: self, selector: #selector(musicUpdate), userInfo: nil, repeats: true)
            
    }
   
    @objc func musicUpdate() {
        if !musicPlayer.isPlaying {
          //  musicPlayer.play()
        }
    }
    @objc func UpdateTimer() {
        
        playerTime += 0.1
        
        TimeLabel.text = "\(String(format: "%.1f", playerTime)) seconds"
        
    }
    @objc func updateConnectivity() {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) connected")
        ref.setValue(true)
        
       // let ref2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
        
        
    }
    @objc func enterAnswer() {
        
        let premAnswer = (AnswerTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
        if premAnswer == "" { return }
        if premAnswer == "-" {return}
        let answer = Int(premAnswer ?? "0")!
        var splitArray = [String]()
        var correctAnswerGiven = false
        var op = 0
        if listOfQuestions[CountMath].contains("+") {
            splitArray = listOfQuestions[CountMath].components(separatedBy: " + ")
           
            op = 1
        } else if listOfQuestions[CountMath].contains("-") {
            splitArray = listOfQuestions[CountMath].components(separatedBy: " - ")
            op = 2
        } else if  listOfQuestions[CountMath].contains("x") {
            splitArray = listOfQuestions[CountMath].components(separatedBy: " x ")
            op = 3
        }
        switch op {
        case 1:
            if Int(splitArray[0])! + Int(splitArray[1])! == answer {
                correctAnswerGiven = true
                CountMath += 1
            } else {
                correctAnswerGiven = false
            }
        case 2:
            if Int(splitArray[0])! - Int(splitArray[1])! == answer {
                correctAnswerGiven = true
                CountMath += 1
            } else {
                
            }
        case 3:
            if Int(splitArray[0])! * Int(splitArray[1])! == answer {
                correctAnswerGiven = true
                CountMath += 1
            } else {
                correctAnswerGiven = false
            }
        default:
            fatalError("Operation Number Out Of Bounds")
        }
        AnswerTextField.text = ""
        
        if correctAnswerGiven {
            QuestionCounterLabel.text = "Correct!"
            QuestionCounterLabel.textColor = UIColor(red: 199/256, green: 255/256, blue: 199/256, alpha: 1)
        } else {
            QuestionCounterLabel.text = "Incorrect!"
            QuestionCounterLabel.textColor = UIColor(red: 247/256, green: 66/256, blue: 72/256, alpha: 1)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.QuestionCounterLabel.text = "Question \(self.CountMath + 1) of \(self.listOfQuestions.count)"
            self.QuestionCounterLabel.textColor = UIColor.black
        }
        if (CountMath) == listOfQuestions.count {
            print("enterAnswer")
            //end game
            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
            ref.child("\(uuid) time").setValue(playerTime)
            timer.invalidate()
            connectivityTimer.invalidate()
             var timeExists = Bool()
            var ap = false
            
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                // ref.observe(.value)  { (snapshot) in
                print("running")
                for child in snapshot.children {
                    
                    let str = String(describing: child)
                    if str.contains("\(opponentUUID) time") {
                        timeExists = true
                        print("fklfwfwefTRUE")
                        break;
                    } else {
                        print("fklfwfwefFALSE")
                        timeExists = false
                    }
                    
                }
                self.readTime(timeExists: timeExists)
            }
            //SO REF.OBSERVE IS A LISTENER AND IT RUNS EVERY TIME SOMEATHING IS ADDED TO THE REF OGHHHHH
           
            //puting readtime in the ref.observe causes fatal error
          
//            ref.observeSingleEvent(of: .value, with: { (snapshot) in
//
//
//
////ref.observeSingleEvent(of: .value, with: { (snapshot) in
//                for child in snapshot.children {
//                   var str = String(describing: child)
//                    if str.contains("\(opponentUUID) time") {
//                        timeExists = true
//                        break
//                    }
//                }
//
////
////                    if snapshot.hasChild("\(opponentUUID) time"){
////
////                        timeExists = true
////
////                    }else{
////
////                       timeExists = false
////                    }
//
//                self.readTime(timeExists: timeExists)
//                })
//
            
//            if !timeExists {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                         let secondVC = storyboard.instantiateViewController(identifier: "WaitingController")
//
//                         secondVC.modalPresentationStyle = .fullScreen
//
//                         //fade to black on next view
//                 self.present(secondVC, animated: true, completion: nil)
//            } else {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                         let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")
//
//                         secondVC.modalPresentationStyle = .fullScreen
//
//                         //fade to black on next view
//                 self.present(secondVC, animated: true, completion: nil)
//            }
           
            
        } else {
            QuestionLabel.text = "\(listOfQuestions[CountMath]) ="
        }
        //deal with wrong/right
       
        
        
       
        
    }
    func readTime(timeExists: Bool) {
        print("readtimeexists \(timeExists)")
        if !timeExists {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "WaitingController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
