//
//  TBViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/1/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
class TBViewController: UIViewController {

    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var tapWeak: UIButton!
    var timer = Timer()
    var connectivityTimer = Timer()
    
    var xArray = [Int]()
    var yArray = [Int]()
    var tapsCount = 0
    var countTB = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/GameData")
        view.backgroundColor = chosenGameColor
        ref.observeSingleEvent(of: .value) { [self] snapshot in
            print(2)
            print(matchPath)
            print(snapshot.value!)
            let value = snapshot.value as! Dictionary<String, Any>
            
           
//            for index in 0..<a.count {
//
//                xArray.append(Int(Array(a.keys)[index])!)
//                //xArray.append(Int(item)!)
//            }
            
        
            self.tapsCount = value.count
          //  tapsCount = xArray.count
//            for item in a.values {
//                yArray.append(item)
//            }
            for index in 1...self.tapsCount {
                print(index)
                let val = value["\(index)a"] as? String ?? "100 100"
            
                let array = val.components(separatedBy: " ")
                self.xArray.append(Int(array[0])!)
               self.yArray.append(Int(array[1])!)
                
            }
//            let seed = matchPath.count
//            xArray.sort()
//            yArray.sort()
//            var gen = SeededRandomNumberGenerator(seed: seed)
//            xArray.shuffle(using: &gen)
//            yArray.shuffle(using: &gen)
//
            playerTime = 0.0
            self.countTB = 0
            let alert = UIAlertController(title: "Game Data (For Testing Purposes)", message: "\(self.xArray) \n \(self.yArray)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateTimer), userInfo: nil, repeats: true)
            
            let connref = Database.database().reference(withPath: "connectWaitTime")
            var waitTime = 5
            connref.observeSingleEvent(of: .value) { snapshot in
                //connref.observe(.value) { [self] snapshot in
                waitTime = snapshot.value as! Int
            }
            self.connectivityTimer = Timer.scheduledTimer(timeInterval: TimeInterval(waitTime), target: self, selector: #selector(self.updateConnectivity), userInfo: nil, repeats: true)
            self.tapLabel.text = "Tap 1 of \(self.tapsCount)"
        }
    }
    
    @objc func UpdateTimer() {
        
        playerTime += 0.1
        
        timeLabel.text = "\(String(format: "%.1f", playerTime)) seconds"
        
    }
    @objc func updateConnectivity() {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) connected")
        ref.setValue(true)
       
        
    }
    @IBAction func Tap(_ sender: UIButton) {
        if tapsCount == countTB {
            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
            ref.child("\(uuid) time").setValue(playerTime)
            timer.invalidate()
            connectivityTimer.invalidate()
             var timeExists = Bool()
           
            
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                // ref.observe(.value)  { (snapshot) in
                
                for child in snapshot.children {
                    
                    let str = String(describing: child)
                    if str.contains("\(opponentUUID) time") {
                        timeExists = true
                        
                        break;
                    } else {
                        
                        timeExists = false
                    }
                    
                }
                self.readTime(timeExists: timeExists)
            }
        } else {
            tapWeak.center.x = CGFloat(xArray[countTB])
            tapWeak.center.y = CGFloat(yArray[countTB])
            
            countTB += 1
            self.tapLabel.text = "Tap \(countTB) of \(tapsCount)"
        }
    }
    func readTime(timeExists: Bool) {
        print("readtimeexists \(timeExists)")
        if !timeExists {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "WaitingController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                     secondVC.modalPresentationStyle = .fullScreen
            
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
