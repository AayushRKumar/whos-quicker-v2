//
//  AppDelegate.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 6/24/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
   
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
            NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIScene.didEnterBackgroundNotification, object: nil)
                //used to be .willdeactivatenotification
        NotificationCenter.default.addObserver(self, selector: #selector(willActivate), name: UIScene.didActivateNotification, object: nil)
        
        return true
    }
   
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        
    
        
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue/\(uuid)")
        ref.removeValue()
        
        let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/status")
        refToCheck.setValue("applicationWillTerminate")
        
        
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        
//        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue/\(uuid)")
//        ref.removeValue()
//
//        let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/status")
//        refToCheck.setValue("applicationDidEnterBackground")
        
        if Auth.auth().currentUser == nil { return }
        //affected by "users"
//        let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\((Auth.auth().currentUser!.uid))")
//
//        refToCheck.observeSingleEvent(of: .value) { snapshot in
//
//            if snapshot.exists() {
                
            let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Status")
            refToCheck.setValue("0")
                
                if XdsaiodaLshareDetectBool {
                    XdsaiodaLshareMatchPath.setValue("cancelled")
                    XdsaiodaLshareBanner.dismiss()
                    ChooseGameViewController().waitingForMatch = false
                }
//            }
//        }
    }
    
    
    @objc func willResignActive(_ notification: Notification) {
        if Auth.auth().currentUser == nil { return }
        //affected by "users"
        //let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\((Auth.auth().currentUser!.uid))")
        
//        refToCheck.observeSingleEvent(of: .value) { snapshot in
//
//            if snapshot.exists() {
                
            let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Status")
            refToCheck.setValue("0")
                
                if XdsaiodaLshareDetectBool {
                    XdsaiodaLshareMatchPath.setValue("cancelled")
                    XdsaiodaLshareBanner.dismiss()
                    ChooseGameViewController().waitingForMatch = false
                }
//            }
//        }

        
        
      
        
    }
  
    func checkDoublePlayer()  {
        if Auth.auth().currentUser == nil { return }
        let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "UsersIGNORE")
        refToCheck.observeSingleEvent(of: .value) { snapshot in
            if snapshot.hasChild("\((Auth.auth().currentUser!.uid))") {
                
                let refToCheck2 = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
                refToCheck2.observeSingleEvent(of: .value) { snapshot2 in
                   
                       
                       
                       
                        guard let dict = snapshot2.value as? Dictionary<String, Any> else {  return }
                        guard let index = dict.index(forKey: "Status") else {  return }
                        
                        let status = dict[index].value as! String
                         
                        if (status == "1") {
                            let ac = UIAlertController(title: "You Are Already Signed in on Another Device!", message: nil, preferredStyle: .alert)
                            let refresh = UIAlertAction(title: "Refresh", style: .default) { [self] _ in
//                                self.checkLoggedIn()
                                
                            }
                            ac.addAction(refresh)
                            UIApplication.shared.windows[0].rootViewController!.present(ac, animated: true, completion: nil)
                     
                            
                        } else {
                           
                        }
                    
                       
                    }
                    
                }
             
            
        }
    }
    
    @objc func willActivate(_ notification: Notification) {
     checkDoublePlayer()
        if Auth.auth().currentUser == nil { return }
        
           //affected by "users"
                let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Status")
                refToCheck.setValue("1")
            
        
       
            
        
    }

    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
      -> Bool {
      return GIDSignIn.sharedInstance().handle(url)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      // ...
      if let error = error {
       print(error)
        return
      }

      guard let authentication = user.authentication else { return }
      let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                        accessToken: authentication.accessToken)
        var isMFAEnabled = false
        Auth.auth().signIn(with: credential) { (authResult, error) in
          if let error = error {
            let authError = error as NSError
            print(authError)
//            if (isMFAEnabled && authError.code == AuthErrorCode.secondFactorRequired.rawValue) {
//              // The user is a multi-factor user. Second factor challenge is required.
//              let resolver = authError.userInfo[AuthErrorUserInfoMultiFactorResolverKey] as! MultiFactorResolver
//              var displayNameString = ""
//              for tmpFactorInfo in (resolver.hints) {
//                displayNameString += tmpFactorInfo.displayName ?? ""
//                displayNameString += " "
//              }
//
//              self.showTextInputPrompt(withMessage: "Select factor to sign in\n\(displayNameString)", completionBlock: { userPressedOK, displayName in
//                var selectedHint: PhoneMultiFactorInfo?
//                for tmpFactorInfo in resolver.hints {
//                  if (displayName == tmpFactorInfo.displayName) {
//                    selectedHint = tmpFactorInfo as? PhoneMultiFactorInfo
//                  }
//                }
//                PhoneAuthProvider.provider().verifyPhoneNumber(with: selectedHint!, uiDelegate: nil, multiFactorSession: resolver.session) { verificationID, error in
//                  if error != nil {
//                    print("Multi factor start sign in failed. Error: \(error.debugDescription)")
//                  } else {
//                    self.showTextInputPrompt(withMessage: "Verification code for \(selectedHint?.displayName ?? "")", completionBlock: { userPressedOK, verificationCode in
//                      let credential: PhoneAuthCredential? = PhoneAuthProvider.provider().credential(withVerificationID: verificationID!, verificationCode: verificationCode!)
//                      let assertion: MultiFactorAssertion? = PhoneMultiFactorGenerator.assertion(with: credential!)
//                      resolver.resolveSignIn(with: assertion!) { authResult, error in
//                        if error != nil {
//                          print("Multi factor finanlize sign in failed. Error: \(error.debugDescription)")
//                        } else {
//                          self.navigationController?.popViewController(animated: true)
//                        }
//                      }
//                    })
//                  }
//                }
//              })
//            } else {
//              self.showMessagePrompt(error.localizedDescription)
//              return
//            }
            // ...
            return
          }
            
            print("signed in")
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(String(describing: Auth.auth().currentUser!.uid))")
            //affected by "users"
            ref.observeSingleEvent(of: .value)  { (snapshot) in
                if snapshot.exists()
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "segueToMain"), object: nil)
                } else {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "checkAccountInDatabse"), object: nil)
                }
            }
           
           
            
          // User is signed in
        
          // ...
        }
      // ...
        
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
       
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}

