//
//  CustomButton.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 9/30/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//


import UIKit

class CustomButton: UIButton {
   
   override init(frame: CGRect) {
       super.init(frame: frame)
       setupButton()
   }
   
   
   required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       setupButton()
   }
   
   
   func setupButton() {
       setShadow()
    
       setTitleColor(.white, for: .normal)
       
    backgroundColor = UIColor(red: 0.32891, green: 0.410275, blue: 0.78137, alpha: 0.85)
       titleLabel?.font = UIFont(name: "Avenir Next Heavy", size: 36)
    titleLabel?.textColor = UIColor.white
       layer.cornerRadius   = 25
      
   }
   
   
   private func setShadow() {
    layer.shadowColor = UIColor.black.cgColor
          layer.shadowOffset = CGSize(width: 0, height: 3)
         layer.shadowOpacity = 1.0
        layer.shadowRadius = 10.0
         layer.masksToBounds = false
   }
   
   
  
}
