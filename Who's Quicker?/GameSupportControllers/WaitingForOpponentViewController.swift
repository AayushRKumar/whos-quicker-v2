//
//  WaitingForOpponentViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/18/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
class WaitingForOpponentViewController: UIViewController {
     var timer = Timer()
    var waitingMatchCounter = 0
    var waitingConnectCounter = 0
    var cc = false
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var waitingimgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        timer.invalidate()
    }
    override func viewWillDisappear(_ animated: Bool) {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) time")
        ref.removeAllObservers()
    }
    override func viewDidAppear(_ animated: Bool) {
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MatchMakingLoading_pyb", withExtension: "gif")!)
            let advTimeGif = UIImage.gifImageWithData(imageData!)
        mainLabel.adjustsFontSizeToFitWidth = true
        waitingMatchCounter = 0
        waitingConnectCounter = 0
        waitingimgView.image = advTimeGif
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.search), userInfo: nil, repeats: true)
        matchOutCome = MatchOutcome.unknown
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) time")
        ref.observe(.value)  { [self] (snapshot) in
            
            print("valye changed")
            if snapshot.exists() {
                print("TIME DOES EXIST")
                self.timer.invalidate()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                         secondVC.modalPresentationStyle = .fullScreen
                
                         //fade to black on next view
                 self.present(secondVC, animated: true, completion: nil)
                
                //segue
                
            }
            
           

            }
    }
    
    @objc func search() {
       
      
            waitingMatchCounter += 1
            
        
            var connExists = Bool()
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) connected")
            ref.observeSingleEvent(of: .value)  { [self] (snapshot) in
                //ref.observe(.value)  { (snapshot) in
                
                
                if !snapshot.exists() {
                    self.waitingConnectCounter += 1
                }
//                    if snapshot.hasChild("\(opponentUUID) connected") {
//
//                        connExists = true
//
//                    }else{
//                        connExists = false
//                    }
              
                }
            
            
            if self.waitingMatchCounter >= 10 {
                //segue
                self.timer.invalidate()
                matchOutCome = MatchOutcome.opponentTookTooMuchTime
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                         secondVC.modalPresentationStyle = .fullScreen
                
                         //fade to black on next view
                 self.present(secondVC, animated: true, completion: nil)
                
            } else if self.waitingConnectCounter >= 5 {
                self.timer.invalidate()
                matchOutCome = MatchOutcome.opponentLeft
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let secondVC = storyboard.instantiateViewController(identifier: "MatchEndController")

                         secondVC.modalPresentationStyle = .fullScreen
                
                         //fade to black on next view
                 self.present(secondVC, animated: true, completion: nil)
            }
        
       
        print(waitingMatchCounter)
        print(waitingConnectCounter)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
