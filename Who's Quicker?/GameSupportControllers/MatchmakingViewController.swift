//
//  MatchmakingViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/14/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
var uuid = ""
var opponentUUID = ""
class MatchmakingViewController: UIViewController {

    var timer = Timer()
    override func viewWillAppear(_ animated: Bool) {
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        
        exitWeak.layer.cornerRadius = 20
        exitWeak.clipsToBounds = true
        exitWeak.titleLabel?.adjustsFontSizeToFitWidth = true
    
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MatchMakingLoading_pyb", withExtension: "gif")!)
            let advTimeGif = UIImage.gifImageWithData(imageData!)
        canShowBanner = false
        loadingImage.image = advTimeGif
      //  isModalInPresentation = true
         
        navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
        
        
      
    }
    
    @IBOutlet weak var exitWeak: UIButton!
    override func viewDidAppear(_ animated: Bool) {
        
         
        
        if !MatchMakingController_rematching {
            mainLabel.text = "Finding a Match!"
            exitWeak.isHidden = false
         let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue")
        
     
         
      //   uuid = NSUUID().uuidString
       // ref.setValue("username here")
         var value = ""
            if GameNamesArray[chosenGameIndex] == "Tap Button" {
                let h = UIScreen.main.bounds.height
                let w = UIScreen.main.bounds.width
                
                value = "\(w) x \(h)"
            }
            
            if !MatchMakingController_challenging   {
                ref.child("\(uuid)").setValue(value)
                
            }
        } else {
            mainLabel.text = "Rematching!"
            exitWeak.isHidden = true
        }
        if  MatchMakingController_challenging {
            mainLabel.text = "Challenging!"
            exitWeak.isHidden = true
        }
            // your code here
        
       
            
        
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.searchForUUID), userInfo: nil, repeats: true)
        
        

     
            
        
            
        
//        var ref: DatabaseReference!
//
//        ref = Database.database().reference()
//      let userID = Auth.auth().currentUser?.uid
//        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
//          // Get user value
//          let value = snapshot.value as? NSDictionary
//          let username = value?["username"] as? String ?? ""
//          let user = username
//
//          // ...
//          }) { (error) in
//            print(error.localizedDescription)
//        }
    }
    @objc func searchForUUID() {
       print("searchforuuidloop")
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            // ref.observe(.value, with: { snapshot in
         
            
            if !snapshot.hasChild(uuid) {
                
                let ref2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches")
                ref2.observeSingleEvent(of: .value) { snapshot2 in
                    // ref2.observe(.value) { snapshot2 in
                    
                    for child in snapshot2.children {
                        var childStr = String(describing: child)
                        var splitArray = childStr.components(separatedBy: "(")
                        childStr = splitArray[1]
                        splitArray = childStr.components(separatedBy: ")")
                        childStr = splitArray[0]
                        
                        if (childStr.contains("\(uuid)")) {
                            splitArray = childStr.components(separatedBy: " match ")
                            if splitArray[1] == uuid {
                                opponentUUID  = splitArray[0]
                                matchPath = "\(opponentUUID) match \(uuid)"
                            } else {
                                opponentUUID  = splitArray[1]
                                matchPath = "\(uuid) match \(opponentUUID)"
                            }
                            
                            
                            
                            
                            self.timer.invalidate()
                            if MatchMakingController_rematching {
                                self.mainLabel.text = "Rematch Time!"
                            } else {
                                
                            
                            self.mainLabel.text = "Match Found!"
                            }
                            if MatchMakingController_challenging {
                                self.mainLabel.text = "Challenge Time!"
                            }
                        //if find a match with their name THEN invalidate
                            MatchMakingController_rematching = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                // Put your code which should be executed with a delay here
                            

                               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let secondVC = storyboard.instantiateViewController(identifier: "CountDownViewController")

                                        secondVC.modalPresentationStyle = .fullScreen
                                secondVC.modalTransitionStyle = .crossDissolve
                                        //fade to black on next view
                                self.present(secondVC, animated: true, completion: nil)
                            }
                            break;
                        }
                        
                  }
                }
          
                
                print("invalid")
//                self.timer.invalidate()

               

            }
        })
        
            //if snapshot
        
    }
    @IBOutlet var mainLabel: UILabel!
    
    @IBOutlet var loadingImage: UIImageView!
    
    
    @IBAction func exit(_ sender: UIButton) {
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.hasChild(uuid) {
                ref.child(uuid).removeValue()
                exitingGame = true
                self.timer.invalidate()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                         let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")
                
                                      secondVC.modalPresentationStyle = .fullScreen
                self.present(secondVC, animated: true, completion: nil)
            } else {
                self.exitWeak.alpha = 0.5
                self.exitWeak.isEnabled = false
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
