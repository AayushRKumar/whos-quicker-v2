//
//  CountDownViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/15/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import AudioToolbox
class CountDownViewController: UIViewController {
    var countDownNumb = 5
    var timer = Timer()
    
    var load = false
    @IBOutlet var countdownLabel: UILabel!
    
    @IBOutlet var username1Label: UILabel!
    
    @IBOutlet var username2Label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        timer.invalidate()
    }
    override func viewWillAppear(_ animated: Bool) {
        view.backgroundColor = chosenGameColor
        countdownLabel.text = "5"
        countdownLabel.adjustsFontSizeToFitWidth = true
        username1Label.adjustsFontSizeToFitWidth = true
        username2Label.adjustsFontSizeToFitWidth = true
        username1Label.text = uuid
        username2Label.text = opponentUUID
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
        load = false
    }
    @objc func countDown() {
        print("CountdownTimer")
        countDownNumb -= 1
        countdownLabel.text = String(countDownNumb)
      //  AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        HapticGenerator.generate(.heavy)
        if (countDownNumb == 0) {
            if load == true { return }
            load = true
            
            timer.invalidate()
        countDownNumb = 5
        
        
        switch GameNamesArray[chosenGameIndex] {
        case "Math":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "MathViewController")

                    secondVC.modalPresentationStyle = .fullScreen
//            secondVC.modalTransitionStyle = .partialCurl
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
           
            break;
        case "Color Matching":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "CMViewController")

                     secondVC.modalPresentationStyle = .fullScreen
             secondVC.modalTransitionStyle = .crossDissolve
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
            break;
        case "Copy Word":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "CWViewController")

                     secondVC.modalPresentationStyle = .fullScreen
             secondVC.modalTransitionStyle = .crossDissolve
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
            break;
        case "Tap Button":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let secondVC = storyboard.instantiateViewController(identifier: "TBViewController")

                     secondVC.modalPresentationStyle = .fullScreen
             secondVC.modalTransitionStyle = .crossDissolve
                     //fade to black on next view
             self.present(secondVC, animated: true, completion: nil)
            break;
        default:
            print("fatal error 98")
            break;
        
        }
        }
    }
    public enum Haptic {
        case light
        case medium
        case heavy
        case none

        @available(iOS 10.0, *)
        var impactStyle: UIImpactFeedbackGenerator.FeedbackStyle? {
            switch self {
            case .light:
                return .light
            case .medium:
                return .medium
            case .heavy:
                return .heavy
            case .none:
                return nil
            }
        }
    }

    open class HapticGenerator: NSObject {

        /**
            Generates a haptic based on the given haptic
            -parameter haptic: The haptic strength to generate when a banner is shown
         */
        open class func generate(_ haptic: Haptic) {
            guard let style = haptic.impactStyle else { return }
            let feedbackGenerator = UIImpactFeedbackGenerator(style: style)
            feedbackGenerator.prepare()
            feedbackGenerator.impactOccurred()
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
