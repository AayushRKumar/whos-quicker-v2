//
//  MatchEndViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/19/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
var exitingGame = Bool()
var MatchMakingController_rematching = Bool()
var MatchMakingController_challenging = Bool()

enum MatchOutcome {
    case win
    case lose
    case opponentLeft
    case opponentTookTooMuchTime
    case unknown
}
enum ReturnPoint {
    case mainVC
    case chooseGameVC
    case playVC
    case unknown
   
}
var returnPoint = ReturnPoint.unknown
var matchOutCome = MatchOutcome.unknown
class MatchEndViewController: UIViewController {
    var oppTime = 0.0
    var observingRefs = [DatabaseReference]()
    @IBOutlet weak var outcomeLabel: UILabel!
    var winColor: UIColor = UIColor()
    var loseColor: UIColor = UIColor()
  
    @IBOutlet weak var playerTimeLabel: UILabel!
    var inRematch = Bool()
    @IBOutlet weak var exitWeak: UIButton!
    @IBOutlet weak var rematchWeak: UIButton!
    @IBOutlet weak var opponentTimeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        //start the animation
        
        //sliding in time labels
        //blinking win/lose label
        
        // Do any additional setup after loading the view.

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        for ref in observingRefs {
            ref.removeAllObservers()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
     view.backgroundColor = chosenGameColor
        observingRefs.removeAll()
        print("AAAAA + \(matchOutCome)")
        rematchWeak.layer.cornerRadius = 20
        rematchWeak.clipsToBounds = true
        exitWeak.layer.cornerRadius = 20
        exitWeak.clipsToBounds = true
        let matchRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) waitingAtEnd")
        matchRef.setValue(true)
        let rematchRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) rematch")
        rematchRef.setValue(false)
        
        playerTimeLabel.adjustsFontSizeToFitWidth = true
        opponentTimeLabel.adjustsFontSizeToFitWidth = true
        if matchOutCome == .unknown {
            
            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) time")
            var val = 0.0
            let refToObserve = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) waitingAtEnd")
            observingRefs.append(refToObserve)
            refToObserve.observe(.value)  { [self] (snapshot) in
                
                guard let bool = snapshot.value as? Bool else { return }
                if !bool {
                    self.rematchWeak.setTitle("Opponent Exited", for: .normal)
                    self.rematchWeak.isEnabled = false
                    self.rematchWeak.alpha = 0.5
                } else {
                    
                   
                   
                }
            }
            let rematchRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) rematch")
            observingRefs.append(rematchRef)
            rematchRef.observe(.value)  { [self] (snapshot) in
                if (snapshot.exists()) {
                let boolRematch = snapshot.value as! Bool
                print("COUNTER BOOL \(boolRematch)")
                if boolRematch {
                    if self.inRematch {
                        matchOutCome = .unknown
                       MatchMakingController_rematching = true
                       
                        self.rematchWeak.setTitle("REMATCH 2/2", for: .normal)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let secondVC = storyboard.instantiateViewController(identifier: "MatchmakingViewController")
                                  
                                secondVC.modalPresentationStyle = .fullScreen
                        secondVC.modalTransitionStyle = .crossDissolve
                                //fade to black on next view
                        self.present(secondVC, animated: true, completion: nil)
                    } else {
                        self.rematchWeak.setTitle("REMATCH 1/2", for: .normal)
                  
                    }
                }
            }
            }
          
            
            ref.observeSingleEvent(of: .value)  { [self] (snapshot) in
                val = snapshot.value as! Double
                print(val)
                self.oppTime = val
    //  self.present(secondVC, animated: true, completion: nil)
    //            let val = ref.value(forKey: "\(opponentUUID) Time") as! Double
    //
                switch GameNamesArray[chosenGameIndex] {
                
                   case "Math":
                    self.winColor = UIColor(red: 199/256, green: 255/256, blue: 199/256, alpha: 1)
                    self.loseColor = UIColor(red: 247/256, green: 66/256, blue: 72/256, alpha: 1)
                    break;
                case "Color Matching":
                    self.winColor = UIColor(red: 67/256, green: 245/256, blue: 126/256, alpha: 1)
                    self.loseColor = UIColor(red: 246/256, green: 72/256, blue: 72/256, alpha: 1)
                    break;
                case "Copy Word":
                    self.winColor =  UIColor(red: 120/256, green: 255/256, blue: 115/256, alpha: 1)
                    self.loseColor = UIColor(red: 255/256, green: 115/256, blue: 120/256, alpha: 1)
                    break;
                case "Tap Button":
                    self.winColor = UIColor(red: 4/256, green: 249/256, blue: 4/256, alpha: 1)
                    self.loseColor = UIColor(red: 249/256, green: 5/256, blue: 4/256, alpha: 1)
                    break;
                default:
                    self.winColor = UIColor.black
                    self.loseColor = UIColor.black
                    break;
                    
                }
                
                if playerTime < val {
                    matchOutCome = .win
                    self.outcomeLabel.text = "You Won!"
                    self.outcomeLabel.textColor = self.winColor
                } else {
                    matchOutCome = .lose
                    self.outcomeLabel.text = "You Lost!"
                    self.outcomeLabel.textColor = self.loseColor
                }
                
                
                
              
                
                self.opponentTimeLabel.text = "\(opponentUUID)'s Time: \(String(format: "%.1f", self.oppTime))"
                self.playerTimeLabel.text = "Your Time: \(String(format: "%.1f", playerTime))"
                
            }
            
        } else {
            // not unknown
            rematchWeak.isEnabled = false
            
            outcomeLabel.textColor = UIColor.black
            opponentTimeLabel.text = "-"
            playerTimeLabel.text = "Your Time: \(String(format: "%.1f", playerTime))"
            switch matchOutCome {
            case .opponentTookTooMuchTime:
                outcomeLabel.text = "Opponent Took Too Much Time"
            case .opponentLeft:
                outcomeLabel.text = "Opponent Left The Match"
            default:
                outcomeLabel.text = "Error - Outcome: \(matchOutCome)"
            }
            
        }
         
        
        
        
    }
    
    @IBAction func rematch(_ sender: UIButton) {
        let ref2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) rematch")
        ref2.setValue(true)
        inRematch = true
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) rematch")
       
        ref.observeSingleEvent(of: .value)  { [self] (snapshot) in
            let bool = snapshot.value as! Bool
            print("REMATCH PRESS BOOL \(bool)")
            if bool {
                matchOutCome = .unknown
                
                self.rematchWeak.setTitle("REMATCH 2/2", for: .normal)
               // DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                
              
                    
              //  }
                MatchMakingController_rematching = true
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//
//                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                    let addRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Set Players Queue/\(matchPath)")
                    if GameNamesArray[chosenGameIndex] == "Tap Button" {
                        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) dimen")
                        ref.observeSingleEvent(of: .value)  { (snapshot) in
                            print(matchPath)
                            let ref_to_remove = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
                                              ref_to_remove.removeValue()
                            let h = UIScreen.main.bounds.height
                            let w = UIScreen.main.bounds.width
//make the dimen in the ref value and read it from there
                            let oppDimen = snapshot.value as! String
                          
                             addRef.setValue("\(w)x\(h) \(oppDimen)")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            
                            }
                        }
                    } else {
                        let ref_to_remove = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
                                          ref_to_remove.removeValue()
                        addRef.setValue(0)
                    
               
               
                    }
                }
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let secondVC = storyboard.instantiateViewController(identifier: "MatchmakingViewController")
                          
                        secondVC.modalPresentationStyle = .fullScreen
                secondVC.modalTransitionStyle = .crossDissolve
                        //fade to black on next view
                self.present(secondVC, animated: true, completion: nil)
                
                
            } else {
                self.rematchWeak.setTitle("REMATCH 1/2", for: .normal)
            }
        }
        
    }
    
    @IBAction func exit(_ sender: UIButton) {
       
        
        exitingGame = true
        if returnPoint == .unknown {
            returnPoint = .playVC
        }
        let rematchRef = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) waitingAtEnd")
        rematchRef.setValue(false)
        let ref2 = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(uuid) rematch")
        ref2.setValue(false)
        
        if (matchOutCome == .win || matchOutCome == .lose) {
            
        
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)/\(opponentUUID) waitingAtEnd")
       
        ref.observeSingleEvent(of: .value)  { [self] (snapshot) in
            matchOutCome = .unknown
            if snapshot.value as! Bool {
            
            } else {
                let matchRef =  Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
                matchRef.removeValue()
            }
        }
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                     let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")
            
                                  secondVC.modalPresentationStyle = .fullScreen
            self.present(secondVC, animated: true, completion: nil)
        } else if matchOutCome == .opponentTookTooMuchTime {
            matchOutCome = .unknown
            let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
           
            ref.observeSingleEvent(of: .value)  { [self] (snapshot) in
                if snapshot.hasChild("\(opponentUUID) waitingAtEnd") {
                    if !(snapshot.childSnapshot(forPath: "\(opponentUUID) waitingAtEnd").value as! Bool) {
                        let matchRef =  Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
                        matchRef.removeValue()
                    }
                }
                
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                     let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")
            
                                  secondVC.modalPresentationStyle = .fullScreen
            self.present(secondVC, animated: true, completion: nil)
        } else if matchOutCome == .opponentLeft {
            matchOutCome = .unknown
            let matchRef =  Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Matches/\(matchPath)")
            matchRef.removeValue()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                     let secondVC = storyboard.instantiateViewController(identifier: "MainViewController")
            
                                  secondVC.modalPresentationStyle = .fullScreen
            self.present(secondVC, animated: true, completion: nil)
        }
            
            
        
        
       
        
                              
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
