//
//  ChooseGameViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/4/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import GameKit
import Firebase
import NotificationBannerSwift
var chosenGameColor = UIColor.red
var chosenGameCell = ChooseGameCell()
var chosenGameIndex = 0
var GameNamesArray = ["Math", "Color Matching", "Tap Button", "Copy Word"]
var GameDescArray  = ["Quick Math Problems.", "Match the color pairs across the screen.", "Tap a moving button quick.", "Copy the word down quickly."]
var imagesArray = [UIImage(named: "Math_graphic_square"), UIImage(named:"ColorMatching_graphic_square"), UIImage(named:"TapButton_graphic_square"), UIImage(named:"CopyWord_graphic")]
var imagesArray2 = [UIImage(systemName: "plus.slash.minus"), UIImage(systemName: "tablecells"), UIImage(systemName: "hand.tap"), UIImage(systemName: "rectangle.and.pencil.and.ellipsis")]
var colorsArray = [UIColor(red: 255/256, green: 199/256, blue: 255/256, alpha: 1), UIColor(red: 256/256, green: 256/256, blue: 256/256, alpha: 1), UIColor(red: 250/256, green: 130/256, blue: 6/256, alpha: 1), UIColor(red: 190/256, green: 255/256, blue: 115/256, alpha: 1)]
 var XdsaiodaLshareBanner = FloatingNotificationBanner()
 var XdsaiodaLshareMatchPath = DatabaseReference()
var XdsaiodaLshareDetectBool = false
class ChooseGameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    var waitingForMatch = false
    
    @IBOutlet var collectionView: UICollectionView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selGameCell", for: indexPath) as! ChooseGameCell
       
        cell.imageView.image = imagesArray2[indexPath.row]
        cell.imageView.contentMode = .scaleAspectFit
        cell.imageView.tintColor = UIColor.black
         
        
        cell.gameTitleLabel.text = GameNamesArray[indexPath.row]
        cell.gameDescLabel.text = GameDescArray[indexPath.row]
        cell.backgroundColor = UIColor.white
    
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
       
        cell.contentView.layer.cornerRadius = 2.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true

            cell.layer.backgroundColor = UIColor.white.cgColor
            cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2)//CGSizeMake(0, 2.0);
            cell.layer.shadowRadius = 3
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
           
        cell.contentView.layer.cornerRadius = 10.0
        cell.layer.cornerRadius = 10.0
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
       // cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
///
    
       
      //  cell.contentView.backgroundColor = UIColor.red
        cell.backgroundColor = colorsArray[indexPath.row]
      //  cell.backgroundColor = colorsArray[0]
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ChooseGameCell", for: indexPath)
//                return header
//    }
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        if exitingGame {
            if returnPoint != .chooseGameVC {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondVC2 = storyboard.instantiateViewController(identifier: "PlanGameViewController")

         secondVC2.modalPresentationStyle = .pageSheet
         self.present(secondVC2, animated: true, completion: nil)
            } else {
                returnPoint = .unknown
                exitingGame = false
            }
        }
        observingRefs.removeAll()
        collectionView.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
      //  collectionView.register(ChooseGameCell.self, forCellWithReuseIdentifier: "selGameCell")
       // UINavigationBar.appearance().backgroundColor = view.backgroundColor
        navigationController?.navigationBar.barTintColor = view.backgroundColor
        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       
        navigationItem.backBarButtonItem = backBarButtton
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 73/256, green: 0/256, blue: 152/256, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isToolbarHidden = true
        
        if #available(iOS 14.0, *) {
         //   GKAccessPoint.shared.isActive = false
        } else {
            // Fallback on earlier versions
        }
       
        // Do any additional setup after loading the view.
    }
  
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 349, height: 150)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        challengedFriend_bool = false
        for ref in observingRefs {
            ref.removeAllObservers()
        }
      
      
    }
    var observingRefs = [DatabaseReference]()
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if challengedFriend_bool {
            
            if #available(iOS 13.0, *) {
                self.isModalInPresentation = true
            }
            if !XdsaiodaLshareBanner.isDisplaying { waitingForMatch = false }
            if !waitingForMatch {
                waitingForMatch = true
                let h = Int(UIScreen.main.bounds.height)
                let w = Int(UIScreen.main.bounds.width)
                 
              
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(challengeFriend_id)/Requests/\(selfUsername):\(GameNamesArray[indexPath.row]):\(w)x\(h)")
            ref.setValue("MatchReq")
                observingRefs.append(ref)
//                let cancel = UIButton(frame: CGRect(x: 100, y: 0, width: 300, height: 50))
//                cancel.setTitle("Cancel Match Request", for: .normal)
//                cancel.backgroundColor = UIColor(red: 255/256, green: 102/256, blue: 102/256, alpha: 1)
//                cancel.titleLabel?.font = UIFont(name: "Avenir Next", size: 26)
                    let banner = FloatingNotificationBanner(title: "Waiting for Friend to Accept or Deny Match.", subtitle: "Tap To Cancel Match Request", leftView: nil, rightView: nil, style: .success)
                banner.dismissOnTap = true
                banner.dismissOnSwipeUp = false
                banner.autoDismiss = false
                banner.onTap = {
                    banner.dismiss()
                    ref.setValue("cancelled")
                    self.waitingForMatch = false
                    XdsaiodaLshareDetectBool = false
                    if #available(iOS 13.0, *) {
                        self.isModalInPresentation = false
                    }
                }
                banner.show()
                XdsaiodaLshareBanner = banner
                XdsaiodaLshareMatchPath = ref
                XdsaiodaLshareDetectBool = true
                ref.observe(.value, with: { (snapshot) in
                    guard let value = snapshot.value as? String else { return }
                    
                    if value == "denied" {
                        banner.titleLabel?.text = "Match Request Denied"
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            banner.dismiss()
                        }
                        self.waitingForMatch = false
                        ref.removeValue()
                        XdsaiodaLshareDetectBool = false
                    }
                    
                    if value == "accepted" {
                        banner.titleLabel?.text = "Match Request Accepted"
                        returnPoint = .chooseGameVC
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            banner.dismiss()
                            MatchMakingController_challenging = true
                            chosenGameIndex = indexPath.row
                            chosenGameColor = colorsArray[indexPath.row]
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let secondVC = storyboard.instantiateViewController(identifier: "MatchmakingViewController")
                                      
                                    secondVC.modalPresentationStyle = .fullScreen
                            secondVC.modalTransitionStyle = .crossDissolve
                                    //fade to black on next view
                            self.present(secondVC, animated: true, completion: nil)
                        }
                        self.waitingForMatch = false
                        ref.removeValue()
                        XdsaiodaLshareDetectBool = false
                       
                    }
                })
            }
        } else {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let secondVC = storyboard.instantiateViewController(identifier: "PlanGameViewController")

                 secondVC.modalPresentationStyle = .pageSheet
        
            
         self.present(secondVC, animated: true, completion: nil)
        var newColor = colorsArray[indexPath.row]
        newColor = newColor.withAlphaComponent(0.5)
        collectionView.cellForItem(at: indexPath)?.backgroundColor = newColor
        
        let cell = collectionView.cellForItem(at: indexPath)! as! ChooseGameCell
        cell.gameTitleLabel.alpha = 0.5
        cell.imageView.alpha = 0.5
        cell.gameDescLabel.alpha = 0.5
        
        chosenGameColor = colorsArray[indexPath.row]
        chosenGameCell = collectionView.cellForItem(at: indexPath)! as! ChooseGameCell
        print(indexPath.row)
        cell.gameTitleLabel.alpha = 1
        cell.imageView.alpha = 1
        cell.gameDescLabel.alpha = 1
        
        chosenGameIndex = indexPath.row
        
         newColor = colorsArray[indexPath.row]
        newColor = newColor.withAlphaComponent(1)
        collectionView.cellForItem(at: indexPath)?.backgroundColor = newColor
        
    }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        var newColor = colorsArray[indexPath.row]
        newColor = newColor.withAlphaComponent(1)
        collectionView.cellForItem(at: indexPath)?.backgroundColor = newColor
        
        
        let cell = collectionView.cellForItem(at: indexPath)! as! ChooseGameCell
        cell.gameTitleLabel.alpha = 1
        cell.imageView.alpha = 1
        cell.gameDescLabel.alpha = 1
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        var newColor = colorsArray[indexPath.row]
        newColor = newColor.withAlphaComponent(0.5)
        collectionView.cellForItem(at: indexPath)?.backgroundColor = newColor
        
        let cell = collectionView.cellForItem(at: indexPath)! as! ChooseGameCell
        cell.gameTitleLabel.alpha = 0.5
        cell.imageView.alpha = 0.5
        cell.gameDescLabel.alpha = 0.5
    }
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        var newColor = colorsArray[indexPath.row]
        newColor = newColor.withAlphaComponent(1)
        collectionView.cellForItem(at: indexPath)?.backgroundColor = newColor
        
        let cell = collectionView.cellForItem(at: indexPath)! as! ChooseGameCell
        cell.gameTitleLabel.alpha = 1
        cell.imageView.alpha = 1
        cell.gameDescLabel.alpha = 1
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
     /Users/AayushRKumar/Desktop/Who's Quicker?/Who's Quicker?/ChooseGameCell.swift
        // Pass the selected object to the new view controller.
    }
    */

}
