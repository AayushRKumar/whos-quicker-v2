//
//  PlanGameViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/6/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import GameKit
import Firebase
var playerTime = 0.0
class PlanGameViewController: UIViewController, GKGameCenterControllerDelegate, GKMatchmakerViewControllerDelegate, GKMatchDelegate   {
    var uuid = ""
    var timer = Timer()
    func matchmakerViewControllerWasCancelled(_ viewController: GKMatchmakerViewController) {
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFailWithError error: Error) {
        
    }
    
   
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFind match: GKMatch) {
        viewController.dismiss(animated: true, completion: nil)
        match.delegate = self
    }
    @IBOutlet var leaderboardsWeak: UIButton!
    @IBOutlet var playNowWeak: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if exitingGame {
            if returnPoint == .playVC {
                returnPoint = .unknown
            }
            
        exitingGame = false
        }
        canShowBanner = true
    }
    override func viewWillAppear(_ animated: Bool) {
       
        view.backgroundColor = chosenGameColor
        leaderboardsWeak.layer.cornerRadius = 20
        leaderboardsWeak.clipsToBounds = true
        leaderboardsWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        leaderboardsWeak.frame = CGRect(x: leaderboardsWeak.frame.minX, y: leaderboardsWeak.frame.minY, width: leaderboardsWeak.frame.width, height: (UIScreen.main.nativeBounds.height / 13))
       
      
        playNowWeak.layer.cornerRadius = 20
        playNowWeak.clipsToBounds = true
        
        playNowWeak.frame = CGRect(x: playNowWeak.frame.minX, y: playNowWeak.frame.minY, width: playNowWeak.frame.width, height: (UIScreen.main.nativeBounds.height / 13))
        navigationController?.navigationBar.barTintColor = view.backgroundColor
        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       
        
        

    }
    
    @IBAction func playNow(_ sender: Any) {
     
        if !(UIDevice.current.isSimulator) {
            print("not simulator")
          if #available(iOS 14.0, *) {
        
            let isKeyboardConnected = GCKeyboard.coalesced != nil
            if isKeyboardConnected {
                
                ac(title: "External Keyboard Detected!", message: "You can't play online with an external keyboard.", buttonText: "OK")
                return
            }
        } else {
            // Fallback on earlier versions
        }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondVC = storyboard.instantiateViewController(identifier: "MatchmakingViewController")
                  
                secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .crossDissolve
                //fade to black on next view
                present(secondVC, animated: true, completion: nil)
//       
//        print("\(GameNamesArray[chosenGameIndex]) Queue")
//        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue")
//        ref.setValue(["playeruuid": "FAKEUUID"])
//
//        ref.childByAutoId()
//        uuid = NSUUID().uuidString
//        ref.child("\(uuid)").setValue("Player")


//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.searchForUUID), userInfo: nil, repeats: true)
        
        
//
//        while timer.isValid { }
//        print("INVALID")
//        var ref: DatabaseReference!
//      
//        ref = Database.database().reference()
//      let userID = Auth.auth().currentUser?.uid
//        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
//          // Get user value
//          let value = snapshot.value as? NSDictionary
//          let username = value?["username"] as? String ?? ""
//          let user = username
//
//          // ...
//          }) { (error) in
//            print(error.localizedDescription)
//        }
//       // presentMatchmaker()
    }
    @objc func searchForUUID() {
        print("searchforuuid32")
        let ref = Database.database().reference(withPath: "\(GameNamesArray[chosenGameIndex])/Queue")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            //ref.observe(.value, with: { snapshot in
          print(snapshot.value as Any)
            
            if !snapshot.hasChild(self.uuid) {
                print("invalid")
                
                    
                self.timer.invalidate()
            }
        })
        
            //if snapshot
        
    }
    func presentMatchmaker() {
      // 1
      guard GKLocalPlayer.local.isAuthenticated else {
        return
      }
      
      // 2
      let request = GKMatchRequest()
      
      request.minPlayers = 2
      request.maxPlayers = 2
      // 3
      request.inviteMessage = "Would like to play \(GameNamesArray[chosenGameIndex])?"
    
      // 4
        guard let vc = GKMatchmakerViewController(matchRequest: request) else {return }
        vc.matchmakerDelegate = self
        
      present(vc, animated: true)
    }
    @IBAction func leaderBoards(_ sender: Any) {
    }
    
    func ac(title: String, message: String, buttonText: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: nil))
        present(alert, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
}
