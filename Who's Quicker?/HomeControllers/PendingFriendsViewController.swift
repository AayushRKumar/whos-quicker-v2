//
//  PendingFriendsViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/31/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
class PendingFriendsViewController: UITableViewController {
    var loadingView = UIImageView()
    var requests = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        navBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 30)!]
        navBar.isHidden = true
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
            tableView.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        navBar.barTintColor = view.backgroundColor
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MatchMakingLoading_pyb", withExtension: "gif")!)
            let advTimeGif = UIImage.gifImageWithData(imageData!)
        var imgView = UIImageView(image: advTimeGif)
        imgView.frame = CGRect(x: (view.frame.width / 2), y: (view.frame.height / 2), width: 300, height: 300)
        view.addSubview(imgView)
        imgView.center.x = (view.frame.width / 2)
        imgView.center.y = (view.frame.height / 2)
        loadingView = imgView
        loadData()
       
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @IBOutlet weak var navBar: UINavigationBar!
    func loadData() {
        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests")
        ref.observeSingleEvent(of: .value) { snapshot in
            guard let dict = snapshot.value as? Dictionary<String, Any> else {
                let label = UILabel(frame: CGRect(x: (self.view.frame.width / 2), y:(self.view.frame.height / 2), width: 350, height: 200))
                label.center.x = (self.view.frame.width / 2)
                label.center.y = (self.view.frame.height / 2)
                label.font = UIFont(name: "Avenir Next", size: 30)
                
                label.numberOfLines = 4
                label.text = "No Pending Requests!"
                self.view.addSubview(label)
                self.loadingView.isHidden = true
                self.navBar.isHidden = false
                return
            }
            
            let keys = dict.keys
            
            for key in keys {
                if dict[dict.index(forKey: key)!].value as! String == "FriendReq" {
                    self.requests.append("\(key)")
                }
            }
            
            if self.requests.count == 0 {
                let label = UILabel(frame: CGRect(x: (self.view.frame.width / 2), y:(self.view.frame.height / 2), width: 350, height: 200))
                label.center.x = (self.view.frame.width / 2)
                label.center.y = (self.view.frame.height / 2)
                label.font = UIFont(name: "Avenir Next", size: 30)
                
                label.numberOfLines = 4
                label.text = "No Pending Requests!"
                self.view.addSubview(label)
                self.navBar.isHidden = false
            }
            self.tableView.reloadData()
            self.loadingView.isHidden = true
            self.navBar.isHidden = false
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requests.count
    }
     
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        let pendingUsername = requests[indexPath.row]
        let deny = UIAlertAction(title: "Deny Request", style: .destructive, handler: { action in
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(pendingUsername)")
            ref.removeValue()
            self.requests.remove(at: indexPath.row)
            tableView.reloadData()
        })
        let accept = UIAlertAction(title: "Accept Request", style: .default, handler: { action in
            let reftoremove = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(pendingUsername)")
            reftoremove.removeValue()
            self.requests.remove(at: indexPath.row)
            tableView.reloadData()
            //affected by "users"
            let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users").queryOrdered(byChild: "Username").queryEqual(toValue: pendingUsername)
           
            ref.observeSingleEvent(of: .value) { snapshot in
               
                let user = snapshot
              
                guard let dict = (user.value as? Dictionary<String, Any>) else { return }
                    
                    
                let id = dict.keys.first!
                    let userFriendRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends")
                    
                    userFriendRef.child("\(id)").setValue(0)
                   
                   
                    let friendFriendsRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(id)/Friends")
                    friendFriendsRef.child("\(Auth.auth().currentUser!.uid)").setValue(0)
                   
                   
                
                    
                    
                
            }
            
            
            
        })
        alert.popoverPresentationController?.sourceView = self.tableView as UIView
        alert.popoverPresentationController?.sourceRect = self.tableView.rectForRow(at: indexPath)
        alert.addAction(deny)
        alert.addAction(accept)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fpendingcell", for: indexPath)
        cell.textLabel?.text = "Friend Request - \(requests[indexPath.row])"
        // Configure the cell...
        cell.backgroundColor = UIColor(red: 23/256, green: 216/256, blue: 213/256, alpha: 1)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   

}
