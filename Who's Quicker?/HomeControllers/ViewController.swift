//
//  ViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 6/24/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import GameKit
import Firebase
import NotificationBannerSwift
import AudioToolbox

var canShowBanner = true
class ViewController: UIViewController, GKGameCenterControllerDelegate {
    var loadedCenter = false;
    var numberOfDots = 3
    var timer: Timer?
    @IBOutlet var button1weak: UIButton!
    
    
    
    @IBOutlet weak var profileWeak: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var button2weak: UIButton!
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        titleLabel.adjustsFontSizeToFitWidth = true
        canShowBanner = true
//        profileWeak.frame = CGRect(x: 20, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height / 6, width: UIScreen.main.bounds.height / 6, height: UIScreen.main.bounds.width / 2.76)
//        
//        friendsWeak.frame = CGRect(x: UIScreen.main.bounds.width - 20 - UIScreen.main.bounds.width / 2.76, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height / 6, width: UIScreen.main.bounds.height / 6, height: UIScreen.main.bounds.width / 2.76)
    
   
        if exitingGame {
            if returnPoint != .mainVC {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondVC2 = storyboard.instantiateViewController(identifier: "ChooseGameViewController")

         secondVC2.modalPresentationStyle = .pageSheet
         self.present(secondVC2, animated: true, completion: nil)
            } else {
                returnPoint = .unknown
                exitingGame = false
            }
        }
        if uuid == "" {
//            let wordList = ["Apple", "Bat", "Cat", "Door", "Egg", "Food", "Gate", "Height", "Igloo", "Jackolantern", "KitKat", "Left", "Moat", "Note", "Operation", "Puzzle", "Quack", "Rabbit", "Sap", "Truck", "Umbrella", "Video", "Whale", "Xray", "Yak", "Zombie"]
//            let word = wordList.randomElement()
//            let rn = Int.random(in: 0...100)
//
            let usernameRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Username")
           
            usernameRef.observeSingleEvent(of: .value) { snapshot in
                
                uuid = snapshot.value as! String
                
                let wordsList2 = ["Heroic", "Awesome", "Epic", "Great", "Honorable", "Unreal", "Fabled", "Renowned", "Famous", "Celebrated", "Grand"]
                self.titleLabel.text = "Who's Quicker?"
                if Int.random(in: 0...1000) == 489 {
                    self.titleLabel.text = "Who's Quicker? \nThe Legendary Player by the name of \(uuid)"
                }
           }
           
        }
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        button1weak.titleLabel?.adjustsFontSizeToFitWidth = true
        button2weak.titleLabel?.adjustsFontSizeToFitWidth = true
         navigationController?.navigationBar.barTintColor = view.backgroundColor
        navigationController?.navigationBar.isTranslucent = false
        
//        let buttonWidthD = TapWeak.frame.width //89
//        let buttonHeightD = TapWeak.frame.height //88
//
//
//
//        let viewWidthD = CGFloat(375) // 375
//
//        let viewHeightD = TapWeak.superview!.bounds.height //667
//
//
//        let xwidth = viewWidthD - buttonWidthD
//        let yheight = viewHeightD - buttonHeightD
//
//
//        var generator = SeededRandomNumberGenerator(seed: 8909089)
//        let xoffset = Int.random(in: 0 ..< Int(xwidth), using: &generator)
//        let yoffset = Int.random(in: 0 ..< Int(yheight), using: &generator)
//
//
//
//
//
//
//
//        TapWeak.center.x = CGFloat(xoffset) + buttonWidthD / 2
//        TapWeak.center.y = CGFloat(yoffset) + buttonHeightD / 2
     
        //        print("VIEW")
//        titleLabel.adjustsFontSizeToFitWidth = true
//        if (!loadedCenter) {
//          timer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
//
//        GKLocalPlayer.local.authenticateHandler = { gcAuthVC, error
//            if GKLocalPlayer.local.isAuthenticated {
//              print("Authenticated to Game Center!")
//                self.afterAuth()
//            } else if let vc = gcAuthVC {
//                self.present(vc, animated: true, completion: nil)
//            }
//            else {
//                self.timer?.invalidate()
//              print("Error authentication to GameCenter: " )
//
//                self.ac(title: "Error authentication to GameCenter: ", message: "\(error?.localizedDescription ?? "none")", button: "OK")
//            }
//          }
//        } else {
//            self.afterAuth()
//        }
        
        loadedCenter = true;
      
        button1weak.isHidden = false;
        button2weak.isHidden = false;
        profileWeak.isHidden = false;
        
        button1weak.layer.cornerRadius = 20
        button1weak.clipsToBounds = true
        
        button2weak.layer.cornerRadius = 20
        button2weak.clipsToBounds = true
        
        profileWeak.layer.cornerRadius = 20
        profileWeak.clipsToBounds = true
        
        
    }
    @objc func updateCounting() {
        numberOfDots += 1
        if numberOfDots == 4 {
            numberOfDots = 0
        }
        var titleString = "Authenticating to Game Center"
        
        if (numberOfDots == 0) {
            
        } else {
            
        
        for n in 1...numberOfDots {
            titleString += "."
        }
        }
       // titleLabel.text = titleString
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let nameRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
        nameRef.observeSingleEvent(of: .value) { snapshot in
            let dict = snapshot.value as! Dictionary<String, Any>
            let name = dict.values[dict.index(forKey: "Username")!] as? String ?? "error"
            selfUsername = name
        }
//        for i in 1...10000 {
         //   let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users")
//        let id = NSUUID().uuidString
//            ref.child("\(NSUUID().uuidString)").setValue(["Username": "test", "Math Wins": 0, "CW Wins": 0, "CM Wins": 0, "TB Wins": 0, "UUID": "\(id)", "Friends" : [], "Suspended": false, "SuspendedMsg" : ""])
//           print(id)
        
        let refToObv = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests")
        var banners = [FloatingNotificationBanner]()
        var wholeMsg = ""
        refToObv.observe(.childAdded, with: { (snapshot) in
            var requestPath = ""
            
            if canShowBanner {
                var typeOfReq = -1
               var title = ""
                if snapshot.value as? String ?? "error" == "FriendReq" {
                    title = "Friend Request from \(String(describing: snapshot.key))"
                typeOfReq = 0
                } else if (snapshot.value as? String ?? "error").contains("MatchReq") {
                     wholeMsg = snapshot.key as! String
                   requestPath = wholeMsg
                    title = "Match Request from \(wholeMsg.split(separator: ":")[0]) - \(wholeMsg.split(separator: ":")[1])"
                    typeOfReq = 1
                } else if (snapshot.value as? String ?? "error").contains("cancelled") {
                    refToObv.child(snapshot.key).removeValue()
                    return
                } else if (snapshot.value as? String ?? "error").contains("denied") {
                    refToObv.child(snapshot.key).removeValue()
                    return
                } else {
                    title = "ERROR - (Error Msg - \(String(describing: snapshot.value as? String))) Please report it to us"
                }
                let banner = FloatingNotificationBanner(title: "\(title)", subtitle: "Click to Accept, Swipe up to Dimiss", style: .success)
                if typeOfReq == 0 {
                    
                } else if typeOfReq == 1 {
                    banner.autoDismiss = false
                 
                    let matchRefToObv = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(requestPath)")
                    matchRefToObv.observe(.value, with: { (snapshot) in
                        guard let value = snapshot.value as? String else { return }
                        if value == "cancelled" {
                            banner.dismiss()
                            matchRefToObv.removeValue()
                           
                        }
                    })
                
                }
           
               
            banner.onTap = {
                if typeOfReq == 0 {
                let reqUsername = snapshot.key
                //affected by "users"
                let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users").queryOrdered(byChild: "Username").queryEqual(toValue: reqUsername)
                    
               
                ref.observeSingleEvent(of: .value) { snapshot in
                    
                    let user = snapshot
                  
                    guard let dict = (user.value as? Dictionary<String, Any>) else { return }
                    
                        
                        
                        
                        
                    let id = dict.keys.first!
                        let userFriendRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends")
                        
                            userFriendRef.child("\(id)").setValue(0)
                       
                        
                        let friendFriendsRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(id)/Friends")
                        friendFriendsRef.child("\(Auth.auth().currentUser!.uid)").setValue(0)
                        
                        let toDeleteRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(reqUsername)")
                        
                        toDeleteRef.removeValue()
                        
                        
                    
                 }
                } else if typeOfReq == 1 {
                    let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(requestPath)")
                    ref.setValue("accepted")
                    
                    let challenge_chosen_game = wholeMsg.split(separator: ":")[1]
                    
                    let addRef = Database.database().reference(withPath: "\(challenge_chosen_game)/Set Players Queue/\(selfUsername) match \(wholeMsg.split(separator: ":")[0])")
                    if challenge_chosen_game == "Tap Button" {
                        let h = Int(UIScreen.main.bounds.height)
                        let w = Int(UIScreen.main.bounds.width)
                        let wh = "\(w)x\(h)"
                        addRef.setValue("\(wholeMsg.split(separator: ":")[2]) \(wh)")
                      
                    } else {
                       
                        addRef.setValue(0)
                    
               
               
                    }
                    MatchMakingController_challenging = true
                    chosenGameIndex = GameNamesArray.firstIndex(of: String(wholeMsg.split(separator: ":")[1]))!
                    chosenGameColor = colorsArray[chosenGameIndex]
                    returnPoint = .mainVC
                //    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.dismiss(animated: false, completion: nil)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let secondVC = storyboard.instantiateViewController(identifier: "MatchmakingViewController")
                              
                            secondVC.modalPresentationStyle = .fullScreen
                    secondVC.modalTransitionStyle = .crossDissolve
                            //fade to black on next view
                    self.present(secondVC, animated: true, completion: nil)
                   // }
                    
                }
            }
            banner.onSwipeUp = {
                if typeOfReq == 1 {
                    banner.dismiss()
                    let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests/\(requestPath)")
                    ref.setValue("denied")
                }
            }
                banners.append(banner)
            }
            let bannerQueueToDisplaySeveralBanners = NotificationBannerQueue(maxBannersOnScreenSimultaneously: 6)
            self.showBanners(banners,in: bannerQueueToDisplaySeveralBanners)
            banners.removeAll()
        })
     
       
        // Do any additional setup after loading the view.
     
        
       
       
        
    }
    func showBanners(_ banners: [FloatingNotificationBanner],
                     in notificationBannerQueue: NotificationBannerQueue) {
        banners.forEach { banner in
            banner.show(
                        queue: notificationBannerQueue,
                        cornerRadius: 8,
                        shadowColor: UIColor(red: 0.431, green: 0.459, blue: 0.494, alpha: 1),
                        shadowBlurRadius: 16,
                        shadowEdgeInsets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        }
    }
    func afterAuth() {
        timer?.invalidate()
        loadedCenter = true;
      //  titleLabel.text = "Who's Quicker?"
        button1weak.isHidden = false;
        button2weak.isHidden = false;
        if #available(iOS 14.0, *) {
//            GKAccessPoint.shared.location = .topLeading
//            GKAccessPoint.shared.isActive = true
//            GKAccessPoint.shared.showHighlights = false
        } else {
            // Fallback on earlier versions
        }
       
    }
    public func ac(title: String, message: String, button: String) {
      
            
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
       present(alert, animated: true)
        
        }
    @IBAction func playSingle(_ sender: Any) {
       
       
    }
    
    @IBAction func playMulti(_ sender: Any) {
      
      
       
       
    }
    
    
    @IBAction func profile(_ sender: Any) {
      
       
    }
    
  
    @IBAction func AnyButtonPress(_ sender: Any) {
    
    }
  
    
}

