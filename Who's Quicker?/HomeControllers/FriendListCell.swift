//
//  FriendListCell.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/18/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit

class FriendListCell: UICollectionViewCell {
    
    @IBOutlet weak var challengeWeak: UIButton!
    @IBOutlet weak var namelabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
}
