//
//  ChooseGameCell.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 7/4/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit

class ChooseGameCell: UICollectionViewCell {
    
    
    @IBOutlet var gameTitleLabel: UILabel!
    
    @IBOutlet var gameDescLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
