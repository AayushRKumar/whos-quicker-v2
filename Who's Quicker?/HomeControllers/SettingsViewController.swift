//
//  SettingsViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 5/9/21.
//  Copyright © 2021 Aayush R Kumar. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var vLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
                vLabel.text = "Version: \(appVersionString)"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func attributions(_ sender: Any) {
        let ac = UIAlertController(title: "ATTRIBUTIONS", message: "The icon “Double Ring” is provided by loading.io", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(ac, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
