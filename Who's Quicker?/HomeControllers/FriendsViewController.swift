//
//  FriendsViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/19/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
var challengeFriend_id = ""
var challengedFriend_bool = false
class FriendsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AVCaptureMetadataOutputObjectsDelegate {
    var friends = [friend]()
    var loadingView = UIImageView()
    var video = AVCaptureVideoPreviewLayer()
    var session = AVCaptureSession()
    var observedRefs = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 30)!]
        navBar.isHidden = true
        observedRefs.removeAll()
       // qrOverlay.frame = CGRect(x: ((UIScreen.main.bounds.width / 2)), y: ((UIScreen.main.bounds.height / 2)), width: 300, height: 300)
        qrOverlay.center.x = (view.frame.width / 2)
        qrOverlay.center.y = (view.frame.height / 2)
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        friendsCollectionView.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)
        navBar.barTintColor = view.backgroundColor
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MatchMakingLoading_pyb", withExtension: "gif")!)
            let advTimeGif = UIImage.gifImageWithData(imageData!)
        let imgView = UIImageView(image: advTimeGif)
        imgView.frame = CGRect(x: (view.frame.width / 2), y: (view.frame.height / 2), width: 300, height: 300)
        view.addSubview(imgView)
        imgView.center.x = (view.frame.width / 2)
        imgView.center.y = (view.frame.height / 2)
        loadingView = imgView
        
        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends")
        ref.observe(.value) { snapshot in
            let label = UILabel(frame: CGRect(x: (self.view.frame.width / 2), y:(self.view.frame.height / 2), width: 350, height: 200))
            guard let dict = snapshot.value as? Dictionary<String, Any> else {
                //say none
                
                label.center.x = (self.view.frame.width / 2)
                label.center.y = (self.view.frame.height / 2)
                label.font = UIFont(name: "Avenir Next", size: 30)
                
                label.numberOfLines = 4
                label.text = "No Friends, why don't you add some with the + button?"
                self.view.addSubview(label)
                self.loadingView.isHidden = true
                self.navBar.isHidden = false
              
                return
            }
            
            let keys = dict.keys
            
            for key in keys {
                let fref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(key)")
                fref.observeSingleEvent(of: .value) { snapshot in
                    guard let fdict = snapshot.value as? Dictionary<String, Any> else { return }
                    guard let nameIndex = fdict.index(forKey: "Username") else { return }
                    guard let statusIndex = fdict.index(forKey: "Status") else { return }
                    guard let uuidIndex = fdict.index(forKey: "UUID") else { return }
                    var stsText = ""
                    let sts = Int(fdict[statusIndex].value as! String)!



                    if sts == 1 {
                        stsText = "Status: Online"

                    }
                    if sts == 0 {
                        stsText = "Status: Offline"

                    }
                    let observingRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(key)/Status")
                    self.observedRefs.append("Users/\(key)/Status")
                    observingRef.observe(.value) { snapshot in
                        let observedSts = snapshot.value! as? String ?? "0"
                        
                      
                        for (index, fr) in self.friends.enumerated() {
                            if fr.id == key {
                                if observedSts == "1" {
                                    stsText = "Status: Online"
                                    
                                }
                                if observedSts == "0" {
                                    stsText = "Status: Offline"
                                    
                                }
                                var newFriend = fr
                                newFriend.statusText = stsText
                                self.friends.remove(at: index)
                                self.friends.insert(newFriend, at: index)
                                self.friendsCollectionView.reloadData()
                                
                            }
                        }
                        
                    }
                    let loadedFriend = friend(name: fdict[nameIndex].value as! String, statusText:  stsText, id: "\(key)", uuidText:  fdict[uuidIndex].value as! String)
                      
                    self.friends.append(loadedFriend)
                    
                    if self.friends.count == keys.count {
                     //   self.friendsCollectionView.reloadData()
                    }
                }
            }
            
            
            self.navBar.isHidden = false
                       self.loadingView.isHidden = true
            
            
          
          
        }
      
       // loadData()
        let requestsRef = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Requests")
        
        requestsRef.observeSingleEvent(of: .value) { snapshot in
            guard let dict = snapshot.value as? Dictionary<String, Any> else { return }
            let keys = dict.keys
            var requestsCount = 0
            for key in keys {
                if dict[dict.index(forKey: key)!].value as! String == "FriendReq" {
                    requestsCount += 1
                }
            }
            self.showBadge(withCount: requestsCount, notificationsButton: self.requestsWeak)
            
        }
       
        // Do any additional setup after loading the view.
        
       
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends")
        ref.removeAllObservers()
        for refIndex in observedRefs {
            let refToDelete = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: refIndex)
            refToDelete.removeAllObservers()
        }
    }
  
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var qrWeak: UIBarButtonItem!
    
  
    @IBOutlet weak var requestsWeak: UIButton!
    
    @IBOutlet weak var friendsNavigationItem: UINavigationItem!
    
    func loadData() {
        
        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends")
        
        ref.observeSingleEvent(of: .value) { snapshot in
            guard let dict = snapshot.value as? Dictionary<String, Any> else {
                //say none
                let label = UILabel(frame: CGRect(x: (self.view.frame.width / 2), y:(self.view.frame.height / 2), width: 350, height: 200))
                label.center.x = (self.view.frame.width / 2)
                label.center.y = (self.view.frame.height / 2)
                label.font = UIFont(name: "Avenir Next", size: 30)
                
                label.numberOfLines = 4
                label.text = "No Friends, why don't you add some with the + button?"
                self.view.addSubview(label)
                self.loadingView.isHidden = true
                self.navBar.isHidden = false
              
                return
            }
            
            let keys = dict.keys
            
            for key in keys {
                let fref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(key)")
                fref.observeSingleEvent(of: .value) { snapshot in
                    guard let fdict = snapshot.value as? Dictionary<String, Any> else { return }
                    guard let nameIndex = fdict.index(forKey: "Username") else { return }
                    guard let statusIndex = fdict.index(forKey: "Status") else { return }
                    guard let uuidIndex = fdict.index(forKey: "UUID") else { return }
                    var stsText = ""
                    let sts = Int(fdict[statusIndex].value as! String)!
                    
                    
                    
                    if sts == 1 {
                        stsText = "Status: Online"
                        
                    }
                    if sts == 0 {
                        stsText = "Status: Offline"
                        
                    }
                    let loadedFriend = friend(name: fdict[nameIndex].value as! String, statusText:  stsText, id: "\(key)", uuidText:  fdict[uuidIndex].value as! String)
                      
                    self.friends.append(loadedFriend)
                    
                    if self.friends.count == keys.count {
                        self.friendsCollectionView.reloadData()
                    }
                }
            }
            
            
            self.navBar.isHidden = false
                       self.loadingView.isHidden = true
            
            
          
          
           
        }
    }
    @IBOutlet weak var friendsCollectionView: UICollectionView!
    
    @IBOutlet weak var qrOverlay: UIImageView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return friends.count
    }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "friendCell", for: indexPath) as! FriendListCell
        // Configure the cell
        cell.namelabel.text = friends[indexPath.row].name
        cell.statusLabel.text = friends[indexPath.row].statusText
        cell.challengeWeak.layer.cornerRadius = 20
        cell.challengeWeak.clipsToBounds = true
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
       
        cell.contentView.layer.cornerRadius = 2.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true

            cell.layer.backgroundColor = UIColor.white.cgColor
            cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
           
        cell.contentView.layer.cornerRadius = 10.0
        cell.layer.cornerRadius = 10.0
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        cell.backgroundColor = UIColor(red: 23/256, green: 216/256, blue: 213/256, alpha: 1)
        cell.challengeWeak.addTarget(FriendsViewController(), action: #selector(challenge(_:)), for: .touchUpInside)
        cell.challengeWeak.tag = indexPath.row
        
        return cell
    }
    @IBAction func challenge(_ sender: UIButton) {
        if friends[sender.tag].statusText == "Status: Offline" {
            let alert = UIAlertController(title: "Player is Not Online Right Now!", message: nil, preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
            return
        }
        let selFriend = friends[sender.tag]
        challengeFriend_id = selFriend.id
        challengedFriend_bool = true
//        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(selFriend.id)/Requests")
//        ref.child("")
//
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let secondVC = storyboard.instantiateViewController(identifier: "ChooseGameViewController")

                 secondVC.modalPresentationStyle = .pageSheet
        
                 //fade to black on next view
         self.present(secondVC, animated: true, completion: nil)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 349, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Options:", message: nil, preferredStyle: .actionSheet)
        let remove = UIAlertAction(title: "Remove Friend", style: .destructive, handler: { action in
          
            let selFriend = self.friends[indexPath.row]
            var refToRemove = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Friends/\(selFriend.id)")
            refToRemove.removeValue()
            refToRemove = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(selFriend.id)/Friends/\(Auth.auth().currentUser!.uid)")
            refToRemove.removeValue()
            self.friends.remove(at: indexPath.row)
            self.friendsCollectionView.reloadData()
        })
        alert.popoverPresentationController?.sourceView = self.friendsCollectionView as UIView
        alert.popoverPresentationController?.sourceRect = self.friendsCollectionView.cellForItem(at: indexPath)!.frame
        alert.addAction(remove)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true)
            
        
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
   
    @IBAction func menu(_ sender: Any) {
    }
    
    @IBAction func addFriend(_ sender: Any) {
        let alertController = UIAlertController(title: "Add Friend", message: "Ask your friend to send you their ID. They can share it from their profile.", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField()
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let enter = UIAlertAction(title: "Enter", style: .default) {
            [weak self] action in
           
            guard let enteredId = alertController.textFields?[0].text else { return }
            for fri in self!.friends {
                if fri.uuidText == enteredId {
                    let ac = UIAlertController(title: "This Person is Already Your Friend!", message: nil, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self?.present(ac, animated: true, completion: nil)
                    
                    return
                }
            }
            self?.sendRequest(enteredId)
          
            
            
        }
        alertController.addAction(enter)
        present(alertController, animated: true)
        
        
    }
    
    func sendRequest(_ id: String) {
        if id == selfID {
            let ac = UIAlertController(title: "You can't friend yourself", message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(ac, animated: true, completion: nil)
            return
        }
        
        let ac = UIAlertController(title: "Searching...", message: nil, preferredStyle: .alert)
       
        
        present(ac, animated: true, completion: nil)
        
         let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users").queryOrdered(byChild: "UUID").queryEqual(toValue: id)
        
        refToCheck.observeSingleEvent(of: .value) { snapshot in
             
             let user = snapshot
           
            guard let dict = (user.value as? Dictionary<String, Any>)?.values.first as? Dictionary<String, Any> else {
                //nil ID
                self.dismiss(animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                let ac = UIAlertController(title: "Couldn't Find a Player With that ID! Make sure the ID is correct!", message: nil, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(ac, animated: true, completion: nil)
                })
                return
            }
            //guard let index = dict.index(forKey: "UUID") else {  return; }
            
//            let userID = dict[index].value as! String
            
           //if requests exists
            if dict.keys.contains("Requests") {
                
            guard let pendingReqs = dict[dict.index(forKey: "Requests")!].value as? Dictionary<String, Any> else { return; }
            for item in pendingReqs {
                
                if item.key == "\(selfUsername)" {
                    self.dismiss(animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    let ac = UIAlertController(title: "Request is Already Pending!", message: nil, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(ac, animated: true, completion: nil)
                    })
                    return
                }
            }
            
            }
            self.dismiss(animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(String(describing: (user.value as? Dictionary<String, Any>)!.keys.first!))/Requests/\(uuid)")
            ref.setValue("FriendReq")
            let ac = UIAlertController(title: "Request Sent Sucessfully!", message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(ac, animated: true, completion: nil)
            })
         }
        
//        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users")
//        //affected by "users"
//
//
//        ref.observeSingleEvent(of: .value) { snapshot in
//            var foundPlayer = false
//            var chosenID = ""
//            var count = 0
//            for item in snapshot.children {
//
//                guard let user = item as? DataSnapshot else { break }
//
//                guard let dict = user.value as? Dictionary<String, Any> else {  break }
//                guard let index = dict.index(forKey: "UUID") else {  break; }
//
//                let userID = dict[index].value as! String
//
//                if dict.keys.contains("Requests") {
//                guard let pendingReqs = dict[dict.index(forKey: "Requests")!].value as? Dictionary<String, Any> else { break; }
//                for item in pendingReqs {
//                    if item.key == "\(selfUsername)" {
//                        self.dismiss(animated: true, completion: nil)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//                        let ac = UIAlertController(title: "Request is Already Pending!", message: nil, preferredStyle: .alert)
//                        ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                        self.present(ac, animated: true, completion: nil)
//                        })
//                        return
//                    }
//                }
//
//                }
//
//                if userID == id {
//                    //found right player
//                    foundPlayer = true
//                    chosenID = user.key
//
//                    break;
//                }
//
//
//
//            }
//            self.dismiss(animated: true, completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//            if foundPlayer {
//
//                let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(chosenID)/Requests/\(uuid)")
//                ref.setValue("FriendReq")
//                let ac = UIAlertController(title: "Request Sent Sucessfully!", message: nil, preferredStyle: .alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                self.present(ac, animated: true, completion: nil)
//            } else {
//
//                let ac = UIAlertController(title: "Couldn't Find a Player With that ID! Make sure the ID is correct!", message: nil, preferredStyle: .alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                self.present(ac, animated: true, completion: nil)
//            }
//
//        })
//        }
            
    }
    var displayingQR = false
   
    @IBAction func scanQR(_ sender: Any) {
    
    
        if displayingQR {
            qrWeak.tintColor = UIColor.white
            session.stopRunning()
            view.layer.sublayers?[(view.layer.sublayers?.firstIndex(of: video))!].removeFromSuperlayer()
           
            session.removeOutput(session.outputs[0])
            session.removeInput(session.inputs[0])
            friendsCollectionView.reloadData()
            displayingQR = false
            qrOverlay.isHidden = true
            return
        }
        qrWeak.tintColor = UIColor.green
        displayingQR = true
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do {
            let input = try AVCaptureDeviceInput(device: (captureDevice)!)
            session.addInput(input)
        }
        catch {
            print("error with scanQR")
        }
       
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [.qr]
        video = AVCaptureVideoPreviewLayer(session: session)
        
        video.frame = view.layer.bounds
         view.layer.addSublayer(video)
        
       
       
        self.view.bringSubviewToFront(qrOverlay)
        qrOverlay.isHidden = false
        
 
        
        session.startRunning()
       
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            if object.type == .qr {
                qrWeak.tintColor = UIColor.white
                session.stopRunning()
                view.layer.sublayers?[(view.layer.sublayers?.firstIndex(of: video))!].removeFromSuperlayer()
                session.removeOutput(session.outputs[0])
                session.removeInput(session.inputs[0])
                friendsCollectionView.reloadData()
                displayingQR = false
                qrOverlay.isHidden = true
                sendRequest(object.stringValue ?? "")
            }
        }
    }
    
    struct friend {
       
        var name: String
        var statusText: String
        var id: String
        var uuidText: String
        
        init(name: String, statusText: String, id: String, uuidText: String){
           
            self.name = name
            self.statusText = statusText
            self.id = id
            self.uuidText = uuidText
            
            
        }
        
    }
    
    //MARK: badge
    let badgeSize: CGFloat = 16
    let badgeTag = 9830384

    func badgeLabel(withCount count: Int) -> UILabel {
        let badgeCount = UILabel(frame: CGRect(x: 0, y: 0, width: badgeSize, height: badgeSize))
        badgeCount.translatesAutoresizingMaskIntoConstraints = false
        badgeCount.tag = badgeTag
        badgeCount.layer.cornerRadius = badgeCount.bounds.size.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(11)
        badgeCount.backgroundColor = .systemRed
        badgeCount.text = String(count)
        return badgeCount
    }
    func showBadge(withCount count: Int, notificationsButton: UIButton) {
        let badge = badgeLabel(withCount: count)
        notificationsButton.addSubview(badge)

        NSLayoutConstraint.activate([
            badge.leftAnchor.constraint(equalTo: notificationsButton.leftAnchor, constant: 73),
            badge.topAnchor.constraint(equalTo: notificationsButton.topAnchor, constant: -2),
            badge.widthAnchor.constraint(equalToConstant: badgeSize),
            badge.heightAnchor.constraint(equalToConstant: badgeSize)
        ])
    }
}
