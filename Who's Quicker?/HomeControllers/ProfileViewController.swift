//
//  ProfileViewController.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 8/17/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit
import Firebase
var selfID = ""
var selfUsername = ""
class ProfileViewController: UIViewController {
    var id = ""
    
    @IBOutlet weak var titleLabel: UILabel!
   
    @IBOutlet weak var mathWinsLabel: UILabel!
    @IBOutlet weak var CMWinsLabel: UILabel!
    @IBOutlet weak var TBWinsLabel: UILabel!
    @IBOutlet weak var CWWinsLabel: UILabel!
    
    @IBOutlet weak var idWeak: UIButton!
    @IBOutlet weak var friendsWeak: UIButton!
    var loadingView = UIImageView()
    
    @IBOutlet weak var signOutWeak: UIButton!
    
    @IBOutlet weak var qrCodeWeak: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.backgroundColor = UIColor(red: 0/256, green: 152/256, blue: 149/256, alpha: 1)

        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MatchMakingLoading_pyb", withExtension: "gif")!)
            let advTimeGif = UIImage.gifImageWithData(imageData!)
        var imgView = UIImageView(image: advTimeGif)
        imgView.frame = CGRect(x: (view.frame.width / 2), y: (view.frame.height / 2), width: 300, height: 300)
        view.addSubview(imgView)
        imgView.center.x = (view.frame.width / 2)
        imgView.center.y = (view.frame.height / 2)
        loadingView = imgView
        
        friendsWeak.layer.cornerRadius = 20
        friendsWeak.clipsToBounds = true
        
        idWeak.layer.cornerRadius = 10
        idWeak.clipsToBounds = true
        
        idWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        loadData()
        
    }
     
    func loadData() {
        let ref = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
        ref.observeSingleEvent(of: .value) { snapshot in
            let dict = snapshot.value as! Dictionary<String, Any>
            let mathW = dict.values[dict.index(forKey: "Math Wins")!] as? Int ?? 0
            let cwW = dict.values[dict.index(forKey: "CW Wins")!] as? Int ?? 0
            let cmW = dict.values[dict.index(forKey: "CM Wins")!] as? Int ?? 0
            let tbW = dict.values[dict.index(forKey: "TB Wins")!] as? Int ?? 0
            let name = dict.values[dict.index(forKey: "Username")!] as? String ?? "error"
            self.id = dict.values[dict.index(forKey: "UUID")!] as? String ?? "error"
            self.mathWinsLabel.text = "Math Wins: \(mathW)"
            self.CWWinsLabel.text = "Copy Word Wins: \(cwW)"
            self.CMWinsLabel.text = "Color Matching Wins: \(cmW)"
            self.TBWinsLabel.text = "Tap Button Wins: \(tbW)"
            self.titleLabel.text = "Profile - \(name)"
            self.loadingView.isHidden = true
            selfID = self.id
            selfUsername = name
            self.mathWinsLabel.isHidden = false
            self.CWWinsLabel.isHidden = false
            self.CMWinsLabel.isHidden = false
            self.TBWinsLabel.isHidden = false
            self.titleLabel.isHidden = false
            self.friendsWeak.isHidden = false
            self.idWeak.isHidden = false
            self.signOutWeak.isHidden = false
            self.qrCodeWeak.isHidden = false;
        }
    }
    @IBAction func copyID(_ sender: Any) {
        if id == "error" {
            idWeak.setTitle("ERROR FETCHING ID!", for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.idWeak.setTitle("TAP TO COPY ID", for: .normal)
            }
        } else {
//            UIPasteboard.general.string = id
//            idWeak.setTitle("Copied!", for: .normal)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                self.idWeak.setTitle("TAP TO COPY ID", for: .normal)
//            }
//c
            
            let activityVC = UIActivityViewController(activityItems: [id], applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.addToReadingList]
            
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true)
            
        }
    }
    
    
    @IBAction func SignOut(_ sender: Any) {
        let alert = UIAlertController(title: "Sign Out?", message: "Are You Sure You Want to Sign Out?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        let yes = UIAlertAction(title: "Yes", style: .destructive) { [self] _ in
            let refToCheck = Database.database(url: "https://who-s-quicker-dc87d.firebaseio.com/").reference(withPath: "Users/\(Auth.auth().currentUser!.uid)/Status")
            refToCheck.setValue("0")
                
                if XdsaiodaLshareDetectBool {
                    XdsaiodaLshareMatchPath.setValue("cancelled")
                    XdsaiodaLshareBanner.dismiss()
                    ChooseGameViewController().waitingForMatch = false
                }
            
        
            do {
                try Auth.auth().signOut()
                  
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let secondVC = storyboard.instantiateViewController(identifier: "SignUpLogInViewController")
                          
                        secondVC.modalPresentationStyle = .fullScreen
                secondVC.modalTransitionStyle = .crossDissolve
                        
                self.present(secondVC, animated: true, completion: nil)
                
                } catch {
                    let ac = UIAlertController(title: "Error Signing Out", message: nil, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(ac, animated: true, completion: nil)
                }
        }
        alert.addAction(yes)
        present(alert, animated: true, completion: nil)
    }
   
    @IBAction func qrCode(_ sender: Any) {
      
        // 1
        let myString = "\(id)"
        // 2
        let data = myString.data(using: String.Encoding.ascii)
        // 3
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // 4
        qrFilter.setValue(data, forKey: "inputMessage")
        // 5
        guard var qrImage = qrFilter.outputImage else { return }
       
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        qrImage = qrImage.transformed(by: transform)
         let qrUIImage = UIImage(ciImage: qrImage)
     
        let ac = UIAlertController(title: "Your ID QR code", message: nil, preferredStyle: .alert)
        let aa = UIAlertAction(title: "Done", style: .default, handler: nil)
        ac.addAction(aa)
        let imgView = UIImageView(image: qrUIImage)
        imgView.frame = CGRect(x: 0, y: 100, width: 270, height: 270)
        ac.view.addSubview(imgView)
        
      
        
        present(ac, animated: true, completion: nil)
    }
    
    @IBAction func tap(_ sender: Any) {
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
