//
//  SettingTableViewCell.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 6/25/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        detailLabel.adjustsFontSizeToFitWidth = true
        titleLabel.adjustsFontSizeToFitWidth = true;
      //  mathSettingsArray.removeAll()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
