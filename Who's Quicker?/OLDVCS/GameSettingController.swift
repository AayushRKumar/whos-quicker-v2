////
////  GameSettingController.swift
////  Who's Quicker?
////
////  Created by Aayush R Kumar on 6/25/20.
////  Copyright © 2020 Aayush R Kumar. All rights reserved.
////
//
//import UIKit
//var mathSettingsArray = [SettingTab]()
//var settingsAnswers = [String]()
//
//class GameSettingController: UITableViewController {
//    var cells = [SettingTableViewCell]()
//    var cellTextFieldArray = [UITextField]()
//    var selectedArray = [SettingTab]()
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//
//        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        // self.navigationItem.rightBarButtonItem = self.editButtonItem
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        settingsAnswers = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
//        NotificationCenter.default.addObserver(self, selector: #selector(GameSettingController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(GameSettingController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
//        mathSettingsArray.removeAll()
//        mathSettingsArray.append(SettingTab(title: "Number of Questions", subTitle: "The Number of Math Questions asked", acceptedAnswers: "NUMB>0", tag: 941248230))
//        mathSettingsArray.append(SettingTab(title: "Number Range Low", subTitle: "The lowest number when asking questions", acceptedAnswers: "NUMB", tag: 34234234))
//        mathSettingsArray.append(SettingTab(title: "Number Range High", subTitle: "The highest number when asking questions", acceptedAnswers: "NUMB", tag: 39939299))
//        mathSettingsArray.append(SettingTab(title: "Operation", subTitle: "'+', '-', 'x', '/', 'random', 'changing'", acceptedAnswers: "MATHOPS", tag: 939482993))
//      //  mathSettingsArray.append(SettingTab(title: "Game Code (optional)", subTitle: "Game Codes are used for Leaderboard Games", acceptedAnswers: "NUMB"))
//        if selectedGame == "Math" {
//
//            selectedArray = mathSettingsArray
//        }
//
////        for _ in 0...selectedArray.count {
////                    let cell = SettingTableViewCell()
////
////                    cells.append(cell)
////                }
//    }
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return selectedArray.count
//    }
//
//    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
//
//        return nil
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingReuseId", for: indexPath) as! SettingTableViewCell
//
//        cell.titleLabel.text = selectedArray[indexPath.row].title
//        cell.detailLabel.text = selectedArray[indexPath.row].subTitle
//        cellTextFieldArray.append(cell.textField)
//
//            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//
//        if selectedArray[indexPath.row].acceptedAnswers.contains("NUMB")   {
//            cell.textField.keyboardType = .numberPad
//            cell.textField.addNumericAccessory(addPlusMinus: true)
//        } else {
//            cell.textField.autocorrectionType = .no
//            cell.textField.keyboardType = .asciiCapable
//        }
//        var arrayIndex = 0
//        for i in 0..<selectedArray.count {
//
//            if (cell.titleLabel.text == selectedArray[i].title) {
//                arrayIndex = i
//                cell.textField.tag = selectedArray[i].tag
//
//            } else {
//                print("\(cell.titleLabel.text) VS \(selectedArray[i].title)")
//            }
//        }
//
//
//        cells.append(cell)
//        return cell
//    }
//    @objc func keyboardWillShow(notification: Notification) {
//        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
//            print("Notification: Keyboard will show")
//
//            let edgeInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
//
//            tableView.contentInset = edgeInset
//            tableView.scrollIndicatorInsets = edgeInset
//        }
//    }
//    @objc func keyboardWillHide(notification: Notification) {
//        print("Notification: Keyboard will hide")
//        let edgeInset = UIEdgeInsets(top: 0, left: 0, bottom: 0.0, right: 0)
//
//        tableView.contentInset = edgeInset
//        tableView.scrollIndicatorInsets = edgeInset
//    }
//    @objc func textFieldDidChange(_ textField: UITextField) {
//      var cellIndex = 0
//       // settingsAnswers.insert(textField.text ?? "", at: cellIndex)
//
//        for i in 0..<selectedArray.count {
//            if (selectedArray[i].tag == textField.tag) {
//                cellIndex = i
//
//            } else {
//                print("\(selectedArray[i].tag) VS \(textField.tag)")
//            }
//        }
//        settingsAnswers.insert(textField.text ?? "", at: cellIndex)
//        print("Inserting \(textField.text) @ \(cellIndex)")
//
//    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//    }
//    public func GameCodeRun() {
//
//    }
//
//
//
//    @IBAction func startAndCheckGame(_ sender: Any) {
//        //check everything
//      //  let cells = getAllCells()
//
//        var invalidList = [String]()
//         var invalidField = false
//
//        var lowRange = 0
//
//        for index in 0..<cells.count {
//            print("1")
//            var text = cells[index].textField.text ?? ""
//                 text = text.trimmingCharacters(in: .whitespacesAndNewlines)
//            let acceptedAnswers = selectedArray[index].acceptedAnswers
////            if (cells[index].titleLabel.text!.contains("optional")) {
////                continue
////            }
//            if (text == "")
//            {
//               // cells[index].textField.layer.borderColor = UIColor.red.cgColor
//                invalidList.append(selectedArray[index].title + " " + text)
//                invalidField = true;
//                continue
//            }
//
//            if (acceptedAnswers.contains("NUMB")) {
//
//            }
//            if (acceptedAnswers.contains(">0")) {
//                if (Int(text) ?? 0 <= 0) {
//                    invalidList.append(selectedArray[index].title + " " + text)
//                    invalidField = true;
//                }
//            }
//            if (acceptedAnswers.contains("MATHOPS")) {
//                if text == "x" || text == "-" || text == "/" || text == "+" || text == "random" || text == "changing" {
//                    print("--------- " + text)
//                } else {
//
//                    invalidList.append(selectedArray[index].title + " " + text)
//                    invalidField = true;
//                }
//            }
//
//            if (selectedGame == "Math") {
//                if index == 1 {
//                    lowRange = Int(text)!
//
//                }
//                if index == 2 {
//                    var highRange = Int(text)!
//                    if lowRange > highRange {
////                        cells[1].textField.layer.borderColor = UIColor.red.cgColor
////                        cells[index].textField.layer.borderColor = UIColor.red.cgColor
//                        invalidList.append(selectedArray[index].title + " " + text)
//                        invalidField = true;
//                    }
//
//
//                }
//            }
//        }
//        cells.removeAll()
//        if invalidField {
//              ac(title: "Invalid", message: "There are one or more invalid fields \(invalidList)", button: "OK")
//            return
//        }
//
//    }
//
//
//    func getAllCells() -> [SettingTableViewCell] {
//
//        var cells = [SettingTableViewCell]()
//        // assuming tableView is your self.tableView defined somewhere
//
//        for aa in 0...tableView.numberOfRows(inSection: 0) {
//            if let cell = tableView.cellForRow(at: NSIndexPath(row: aa, section: 0) as IndexPath) as? SettingTableViewCell {
//                print(cell.titleLabel.text)
//
//               cells.append(cell)
//            }
//        }
//
//     return cells
//     }
//    /*
//    // Override to support conditional editing of the table view.
//    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the specified item to be editable.
//        return true
//    }
//    */
//
//    /*
//    // Override to support editing the table view.
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            // Delete the row from the data source
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//        }
//    }
//    */
//
//    /*
//    // Override to support rearranging the table view.
//    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
//
//    }
//    */
//
//    /*
//    // Override to support conditional rearranging of the table view.
//    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the item to be re-orderable.
//        return true
//    }
//    */
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//    func ac(title: String, message: String, button: String) {
//
//
//
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
//       present(alert, animated: true)
//
//        }
//}
