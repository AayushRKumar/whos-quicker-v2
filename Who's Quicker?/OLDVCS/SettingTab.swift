
//  SettingTab.swift
//  Who's Quicker?
//
//  Created by Aayush R Kumar on 6/25/20.
//  Copyright © 2020 Aayush R Kumar. All rights reserved.
//

import Foundation
 class SettingTab {
 var title: String
 var subTitle: String
    var acceptedAnswers: String
    var tag: Int
    init(title: String, subTitle: String, acceptedAnswers: String, tag: Int) {
    self.title = title
    self.subTitle = subTitle
        self.acceptedAnswers = acceptedAnswers
        self.tag = tag
 }
}
